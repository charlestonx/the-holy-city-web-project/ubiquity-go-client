package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

/*
*
Querying transaction by ID.

Env variables:
 1. UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
 2. UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
 3. UBI_PROTOCOL - optional, protocol e.g. ethereum (bitcoin by default)

Flags:

	-txID string
	      Transaction ID
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and protocol
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Use ubiquity.ProtocolsAPI.GetProtocols(ctx _context.Context) to fetch all supported protocols
	// See examples/protocols-overview
	var pl string
	if pl = os.Getenv("UBI_PROTOCOL"); pl == "" {
		pl = "bitcoin"
	}

	txID := getTxID()
	tx, resp, err := apiClient.TransactionsAPI.GetTxByHash(ctx, pl, "mainnet", txID).Execute()
	if err != nil {
		panic(fmt.Errorf("failed to get %s transaction by ID '%s': got status '%s' and error '%v'",
			strings.Title(pl), txID, resp.Status, err))
	}

	examples.PrintTxWithEvents(tx)
}

func getTxID() string {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	txID := fs.String("txID", "", "Transaction ID")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse transaction querying flags: %v", err))
	}
	fs.VisitAll(func(f *flag.Flag) {
		if f.Value.String() == "" {
			fs.SetOutput(os.Stderr)
			_, _ = fmt.Fprintln(os.Stderr, "All of the flags are required:")
			fs.PrintDefaults()
			os.Exit(1)
		}
	})
	return *txID
}
