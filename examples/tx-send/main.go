package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"math/big"
	"os"
	"time"

	"github.com/gagliardetto/solana-go"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
	ubiquityTx "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/tx"
	ubiquityWs "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/ws"
)

/**
Submitting a transaction and waiting for its confirmation.
*Important* for BTC, any amount left after sending to destination and change addresses is automatically paid as fee.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
	3) UBI_PROTOCOL - optional, protocol e.g. ethereum (bitcoin by default)

Flags for Bitcoin:
  -changeAddr string
        Change address for the amount left after transferring transaction value and leaving a fee
  -changeAmount int
        Change amount, sat.
  -destinationAddr string
        Destination address
  -destinationAmount int
        Destination amount, sat.
  -inputIndex uint
        Input index
  -inputTx string
        Input transaction
  -network string
        Network
  -privateKey string
        Private key

Flags for Ethereum:
  -destinationAddr string
        Destination address
  -destinationAmount int
        Destination amount in wei
  -network string
        Network
  -nonce uint (optional)
        Nonce (current count of transactions)
  -maxPriorityFeePerGas uint (optional)
        Max priority fee per gas (as introduced with EIP1559)
  -maxFeePerGas uint (optional)
        Max fee per gas
  -privateKey string
        Private key

Flags for Polkadot:
  -destinationAddr string
        Destination address
  -destinationAmount int
        Destination amount
  -network string
        Network
  -tip uint
		Tip to be included in the transaction
  -seedPhrase string
        Seed phrase

Flags for Solana:
  -destinationAddr string
        Destination address
  -destinationAmount int64
        Destination amount in Lamports
  -network string
        Network (mainnet | testnet)
  -privateKeyPath string
        Path to Solana private key file

Flags for Polygon:
  -destinationAddr string
        Destination address
  -destinationAmount int
        Destination amount in Wei
  -network string
        Network
  -nonce uint (optional)
        Nonce (current count of transactions)
  -maxPriorityFeePerGas uint (optional)
        Max priority fee per gas (as introduced with EIP1559)
  -maxFeePerGas uint (optional)
        Max fee per gas
  -privateKey string
        Private key
*/

const (
	txWaitDuration = 15 * time.Minute
)

func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and protocol
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Use ubiquity.ProtocolsAPI.GetProtocols(ctx _context.Context) to fetch all supported protocols
	// See examples/protocols-overview
	var pl string
	if pl = os.Getenv("UBI_PROTOCOL"); pl == "" {
		pl = "bitcoin"
	}

	txService := ubiquityTx.NewService(apiClient.TransactionsAPI, apiClient.GasEstimatorAPI)
	var network, address, txID string
	switch pl {
	case "bitcoin":
		network, address, txID = sendBTC(ctx, txService)
	case "ethereum":
		network, address, txID = sendETH(ctx, txService)
	case "polkadot":
		network, address, txID = sendDOT(ctx, txService)
	case "polygon":
		network, address, txID = sendMATIC(ctx, txService)
	case "solana":
		network, address, txID = sendSOL(ctx, txService)
	default:
		panic(fmt.Errorf("protocol '%s' is not supported yet", pl))
	}

	wsClient, err := ubiquityWs.NewClient(&ubiquityWs.Config{
		Platform: pl,
		Network:  network,
		APIKey:   accessToken,
	})
	if err != nil {
		panic(fmt.Errorf("failed to create a WS client: %v", err))
	}
	defer func() {
		if err := wsClient.Close(); err != nil {
			panic(fmt.Errorf("failed to close websocket connection: %w", err))
		}
	}()

	waitForTxConfirmation(wsClient, address, txID)
}

func sendBTC(ctx context.Context, txService *ubiquityTx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	inputTx := fs.String("inputTx", "", "Input transaction")
	inputIndex := fs.Uint("inputIndex", 0, "Input index")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Int64("destinationAmount", 0, "Destination amount, sat.")
	changeAddr := fs.String("changeAddr", "", "Change address")
	changeAmount := fs.Int64("changeAmount", 0, "Change amount, sat.")
	privKey := fs.String("privateKey", "", "Private key")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse BTC transaction flags: %v", err))
	}
	fs.VisitAll(createRequiredFlagsChecker(fs))

	result, err := txService.SendBTC(ctx, &ubiquityTx.BitcoinTransaction{
		Network: *network,
		From:    []ubiquityTx.TxInput{{Source: *inputTx, Index: uint32(*inputIndex)}},
		To: []ubiquityTx.TxOutput{
			{Destination: *destinationAddr, Amount: *destinationAmount},
			{Destination: *changeAddr, Amount: *changeAmount},
		},
		PrivateKey: *privKey,
	})
	if err != nil {
		panic(fmt.Errorf("failed to send BTC transaction: %v", err))
	}

	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxID
}

func sendETH(ctx context.Context, txService *ubiquityTx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Float64("destinationAmount", 0, "Destination amount")
	privKey := fs.String("privateKey", "", "Private key")
	nonce := fs.String("nonce", "", "Account nonce (optional)")
	maxPriorityFeePerGas := fs.String("maxPriorityFeePerGas", "", "Max priority fee per gas (optional)")
	maxFeePerGas := fs.String("maxFeePerGas", "", "Max fee per gas (optional)")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse ETH transaction flags: %v", err))
	}

	tx := &ubiquityTx.EthereumTransaction{
		Network:    *network,
		PrivateKey: *privKey,
		To:         *destinationAddr,
		Amount:     big.NewFloat(*destinationAmount),
	}

	if *nonce != "" {
		nonceVal, _ := new(big.Int).SetString(*nonce, 10)
		tx.Nonce = toPointer(nonceVal.Int64())
	}

	if *maxFeePerGas != "" {
		tx.MaxFeePerGas, _ = new(big.Int).SetString(*maxFeePerGas, 10)
	}

	if *maxPriorityFeePerGas != "" {
		tx.MaxPriorityFeePerGas, _ = new(big.Int).SetString(*maxPriorityFeePerGas, 10)
	}

	result, err := txService.SendETH(ctx, tx)
	if err != nil {
		panic(fmt.Errorf("failed to send ETH transaction: %v", err))
	}
	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxID
}

func sendDOT(ctx context.Context, txService *ubiquityTx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Int64("destinationAmount", 0, "Destination amount")
	tip := fs.Uint64("tip", 0, "Tip to be included in the transaction")
	seedPhrase := fs.String("seedPhrase", "", "Seed phrase")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse DOT transaction flags: %v", err))
	}
	fs.VisitAll(createRequiredFlagsChecker(fs))

	result, err := txService.SendDOT(ctx, &ubiquityTx.PolkadotTransaction{
		Network:    *network,
		SeedPhrase: *seedPhrase,
		To:         *destinationAddr,
		Amount:     big.NewInt(*destinationAmount),
		Tip:        *tip,
	})
	if err != nil {
		panic(fmt.Errorf("failed to send DOT transaction: %v", err))
	}
	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxHash
}

func sendSOL(ctx context.Context, txService *ubiquityTx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Int64("destinationAmount", 0, "Destination amount")
	privateKeyPath := fs.String("privateKeyPath", "", "Path to solana private key")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse SOL transaction flags: %v", err))
	}
	fs.VisitAll(createRequiredFlagsChecker(fs))

	privateKey, err := solana.PrivateKeyFromSolanaKeygenFile(*privateKeyPath)
	if err != nil {
		panic(fmt.Errorf("failed to load Solana private key from file: %w", err))
	}

	result, err := txService.SendSOL(ctx, &ubiquityTx.SolanaTransaction{
		Network:        *network,
		From:           privateKey,
		To:             *destinationAddr,
		AmountLamports: *destinationAmount,
	})
	if err != nil {
		panic(fmt.Errorf("failed to send SOL transaction: %v", err))
	}
	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxHash
}

func sendMATIC(ctx context.Context, txService *ubiquityTx.UbiquityTransactionService) (string, string, string) {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	destinationAddr := fs.String("destinationAddr", "", "Destination address")
	destinationAmount := fs.Float64("destinationAmount", 0, "Destination amount")
	privKey := fs.String("privateKey", "", "Private key")
	nonce := fs.String("nonce", "", "Account nonce (optional)")
	maxPriorityFeePerGas := fs.String("maxPriorityFeePerGas", "", "Max priority fee per gas (optional)")
	maxFeePerGas := fs.String("maxFeePerGas", "", "Max fee per gas (optional)")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse MATIC transaction flags: %v", err))
	}

	tx := &ubiquityTx.PolygonTransaction{
		Network:    *network,
		PrivateKey: *privKey,
		To:         *destinationAddr,
		Amount:     big.NewFloat(*destinationAmount),
	}

	if *nonce != "" {
		nonceVal, _ := new(big.Int).SetString(*nonce, 10)
		tx.Nonce = toPointer(nonceVal.Int64())
	}

	if *maxFeePerGas != "" {
		tx.MaxFeePerGas, _ = new(big.Int).SetString(*maxFeePerGas, 10)
	}

	if *maxPriorityFeePerGas != "" {
		tx.MaxPriorityFeePerGas, _ = new(big.Int).SetString(*maxPriorityFeePerGas, 10)
	}

	result, err := txService.SendMATIC(ctx, tx)
	if err != nil {
		panic(fmt.Errorf("failed to send MATIC transaction: %v", err))
	}
	fmt.Printf("Transaction Hash = %s and ID = %s\n", result.TxHash, result.TxID)

	return *network, *destinationAddr, result.TxID
}

func createRequiredFlagsChecker(fs *flag.FlagSet) func(f *flag.Flag) {
	return func(f *flag.Flag) {
		if f.Value.String() == "" {
			fs.SetOutput(os.Stderr)
			_, _ = fmt.Fprintln(os.Stderr, "All of the flags are required:")
			fs.PrintDefaults()
			os.Exit(1)
		}
	}
}

func waitForTxConfirmation(wsClient *ubiquityWs.Client, address, txID string) {
	subID, txs, err := wsClient.SubscribeTxs(&ubiquityWs.TxsFilter{Addresses: []string{address}})
	if err != nil {
		panic(fmt.Errorf("failed to subscribe to transactions: %v", err))
	}
	fmt.Println("Subscribed for transactions of address", address)

	startedAt := time.Now()
	fmt.Println("Waiting for transaction confirmation...")
	newBlocks := make(map[string]bool) // This is just for fancy output
	for {
		select {
		case tx, ok := <-txs:
			if !ok {
				// The subscription would close if we close a client or get disconnected
				panic(errors.New("the subscription was closed"))
			}

			blockId := tx.GetBlockId()
			if !newBlocks[blockId] {
				newBlocks[blockId] = true
				fmt.Printf("Got new block #%s, checking...\n", blockId)
			}

			if tx.GetId() == txID || matchExtHash(tx, txID) {
				fmt.Printf("Transaction was confirmed under block #%s!\n", blockId)
				// Can be omitted if you close a client
				if err := wsClient.UnsubscribeTxs(subID); err != nil {
					panic(err)
				}
				return
			}
		case <-time.After(time.Since(startedAt.Add(txWaitDuration))):
			panic(fmt.Errorf("timed out while waiting for transaction confirmation"))
		}
	}
}

// matchExtHash checks if txID matches the extrinsicHash in the tx's meta field, if present
func matchExtHash(tx *ubiquityWs.Tx, txID string) bool {
	metaInterface := tx.Meta
	if metaInterface == nil {
		return false
	}

	extrinsicHash, ok := (*metaInterface)["extrinsicHash"]
	if !ok {
		return false
	}

	extHashStr, ok := extrinsicHash.(string)
	if !ok {
		return false
	}

	return extHashStr == txID
}

func toPointer[T any](a T) *T {
	return &a
}
