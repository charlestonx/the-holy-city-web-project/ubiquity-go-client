package main

import (
	"context"
	"flag"
	"fmt"
	"os"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
	ubiquityTx "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/tx"
)

/**
Sign and (optionally) submit a Polkadot transaction and wait for its confirmation.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)

Flags:
  -rawTx string
        Raw transaction returned by Ubiquity's /tx/create endpoint
  -rawSigningPayload string
        Raw signing payload returned by Ubiquity's /tx/create endpoint
  -network string
		Network
  -seedPhrase string
        Seed phrase
  -send bool
		If true, sends the signed transaction to the network
*/

func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and protocol
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	fs := flag.NewFlagSet("", flag.ContinueOnError)
	network := fs.String("network", "", "Network")
	rawTx := fs.String("rawTx", "", "Raw transaction returned by Ubiquity's /tx/create endpoint")
	rawSigningPayload := fs.String("rawSigningPayload", "", "Raw signing payload returned by Ubiquity's /tx/create endpoint")
	seedPhrase := fs.String("seedPhrase", "", "Seed phrase")
	send := fs.Bool("send", false, "If true, sends the signed transaction to the network")

	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse DOT transaction flags: %v", err))
	}
	fs.VisitAll(createRequiredFlagsChecker(fs))

	var networkId ubiquityTx.PolkadotNetwork
	switch *network {
	case "mainnet":
		networkId = ubiquityTx.PolkadotMainnet
	case "westend":
		networkId = ubiquityTx.PolkadotWestend
	default:
		panic(fmt.Errorf("network %s not supported! Only mainnet and westend are supported", *network))
	}

	rawSignedTx, err := ubiquityTx.SignPolkadotTransaction(*seedPhrase, *rawTx, *rawSigningPayload, networkId)
	if err != nil {
		panic(fmt.Errorf("failed to sign DOT transaction: %w", err))
	}

	fmt.Println("Raw signed transaction:", rawSignedTx)

	if !*send {
		return
	}

	txReceipt, resp, err := apiClient.TransactionsAPI.
		TxSend(ctx, "polkadot", *network).
		SignedTx(ubiquity.SignedTx{Tx: rawSignedTx}).
		Execute()

	if err != nil {
		panic(fmt.Errorf("Ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err))
	}

	fmt.Println("transaction successfully submitted with hash ", txReceipt.GetId())
}

func createRequiredFlagsChecker(fs *flag.FlagSet) func(f *flag.Flag) {
	return func(f *flag.Flag) {
		if f.Value.String() == "" {
			fs.SetOutput(os.Stderr)
			_, _ = fmt.Fprintln(os.Stderr, "All of the flags are required:")
			fs.PrintDefaults()
			os.Exit(1)
		}
	}
}
