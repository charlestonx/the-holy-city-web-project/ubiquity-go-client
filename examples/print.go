package examples

import (
	"fmt"
	"time"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

func PrintTx(tx ubiquity.Tx) {
	fmt.Printf(`[Transaction data: ID = '%s', 
			date = %v, 
			block id = '%s',
			status = %s, 
			event count = %d]`,
		tx.GetId(),
		time.Unix(tx.GetDate(), 0),
		getStringValue(tx.BlockId),
		tx.GetStatus(),
		*tx.NumEvents,
	)
	fmt.Println()
}

func PrintTxOutput(txOutput ubiquity.TxOutputsData) {
	mined := txOutput.GetMined()
	fmt.Printf(`[Transaction data: ID = '%s', 
			output index = %d, 
			is spent = '%v',
			value = %d]`,
		mined.GetTxId(),
		mined.GetIndex(),
		txOutput.GetIsSpent(),
		txOutput.GetValue(),
	)
	fmt.Println()
}

func PrintTxWithEvents(tx ubiquity.Tx) {
	fmt.Printf(`
		Transaction data: ID = '%s',
		date = %v, 
		block_id = '%s', 
		status = %s, 
		events = [`,
		tx.GetId(),
		time.Unix(tx.GetDate(), 0),
		tx.GetBlockId(),
		tx.GetStatus(),
	)

	for _, event := range tx.Events {
		fmt.Printf(`{Event ID = '%s',
			Source address: '%s'
			Destination address: '%s'
			amount: %d
			protocol decimals: %d}`,
			*event.Id,
			getStringValue(event.Source),
			getStringValue(event.Destination),
			event.Amount.Get(),
			*event.Decimals.Get(),
		)
	}
	fmt.Println("]")
}

func getStringValue(str ubiquity.NullableString) string {
	if str.IsSet() {
		return *str.Get()
	}
	return ""
}
