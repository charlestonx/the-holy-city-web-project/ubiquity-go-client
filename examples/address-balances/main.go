package main

import (
	"bytes"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"os"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

/**
Getting address balance and iterating over transactions.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
	3) UBI_PROTOCOL - optional, protocol e.g. ethereum (bitcoin by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and protocol
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Use ubiquity.ProtocolsAPI.GetProtocols(ctx _context.Context) to fetch all supported protocols
	// See examples/protocols-overview
	var protocol string
	if protocol = os.Getenv("UBI_PROTOCOL"); protocol == "" {
		protocol = "bitcoin"
	}

	var address string
	address = getAddress()
	// Getting a balances for given address
	balances, resp, err := apiClient.AccountsAPI.GetListOfBalancesByAddress(ctx, protocol, "mainnet", address).Execute()
	if err != nil {
		panic(fmt.Errorf("failed to get a balances for given address %s, status: '%s' and error '%v'", address, resp.Status, err))
	}
	fmt.Printf("Address '%s' has '%d' currencies count\n", address, len(balances))
	resultStr := ""
	if len(balances) > 0 {

		fmt.Println("Printing several balances")
		resultStr += "["
		for i := 0; i < len(balances) && i < 3; i++ {
			balance := balances[i]
			formatStr := "{\n"
			formatStr += "%s"
			formatStr += "\"confirmed_balance\": \"%s\",\n\"pending_balance\": \"%s\",\n\"confirmed_nonce\": %d,\n\"confirmed_block\": %d\n"
			formatStr += "}%s"
			setComma := ""
			if i < 2 {
				setComma = ","
			}
			resultStr += fmt.Sprintf(formatStr, currencyToStr(balance.GetCurrency()), balance.GetConfirmedBalance(), balance.GetPendingBalance(),
				balance.GetConfirmedNonce(), balance.GetConfirmedBlock(), setComma)
		}
		resultStr += "]"

		fmt.Println(jsonPrettyPrint(resultStr))
	}
}
func getAddress() string {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	address := fs.String("address", "", "address")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse address flags: %v", err))
	}
	fs.VisitAll(func(f *flag.Flag) {
		if f.Value.String() == "" {
			fs.SetOutput(os.Stderr)
			_, _ = fmt.Fprintln(os.Stderr, "All of the flags are required:")
			fs.PrintDefaults()
			os.Exit(1)
		}
	})
	return *address
}

func currencyToStr(currency ubiquity.Currency) string {
	formatStr := "\"currency\": {\n\"asset_path\": \"%s\",\n\"symbol\": \"%s\",\n\"name\": \"%s\",\n\"decimals\": %d,\n\"type\": \"%s\"\n},\n"
	actualCurr := currency.GetActualInstance()
	switch actualCurr.(type) {
	case *ubiquity.NativeCurrency:
		native := actualCurr.(*ubiquity.NativeCurrency)
		return fmt.Sprintf(formatStr,
			native.GetAssetPath(), native.GetSymbol(), native.GetName(), native.GetDecimals(), native.GetType())
	case *ubiquity.TokenCurrency:
		token := actualCurr.(*ubiquity.TokenCurrency)
		return fmt.Sprintf(formatStr,
			token.GetAssetPath(), token.GetSymbol(), token.GetName(), token.GetDecimals(), token.GetType())
	case *ubiquity.SmartTokenCurrency:
		smartToken := actualCurr.(*ubiquity.SmartTokenCurrency)
		return fmt.Sprintf(formatStr,
			smartToken.GetAssetPath(), smartToken.GetSymbol(), smartToken.GetName(), smartToken.GetDecimals(), smartToken.GetType())
	default:
		return fmt.Sprintf("actual instance = %T", actualCurr)
	}
}

func jsonPrettyPrint(in string) string {
	var out bytes.Buffer
	err := json.Indent(&out, []byte(in), "", " ")
	if err != nil {
		return in
	}
	return out.String()
}
