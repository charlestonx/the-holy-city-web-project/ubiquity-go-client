package main

import (
	"context"
	"fmt"
	"os"

	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

/*
*
Get UTXO by account address.

Env variables:
 1. UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
 2. UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
 3. UBI_PROTOCOL - optional, protocol e.g. ethereum (bitcoin by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	config := ubiquity.NewConfiguration()

	// You can *optionally* set a custom server/endpoint or it will use the default that its prod
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Use ubiquity.ProtocolsAPI.GetProtocols(ctx _context.Context) to fetch all supported protocols
	// See examples/protocols-overview
	var pl string
	if pl = os.Getenv("UBI_PROTOCOL"); pl == "" {
		pl = "bitcoin"
	}
	network := "mainnet"
	btcAccountAddress := "14m3sd9HCCFJW4LymahJCKMabAxTK4DAqW"

	// Paginating with 10 items in the response
	var limit int32 = 10

	executor := apiExecutor{
		apiClient: apiClient,
		token:     accessToken,
	}

	_, continuation1, err := executor.executeGetTxOutput(pl, network, btcAccountAddress, "", limit)
	if err != nil {
		return
	}

	if continuation1 != "" {
		executor.executeGetTxOutput(pl, network, btcAccountAddress, continuation1, limit)
	}
}

type apiExecutor struct {
	token     string
	apiClient *ubiquity.APIClient
}

func (api apiExecutor) executeGetTxOutput(protocol, network, accountAddress, continuation string, limit int32) (ubiquity.TxOutputs, string, error) {
	// Setting the access token or api key to the context
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, api.token)

	req := api.apiClient.AccountsAPI.
		GetUTXOByAccount(ctx, protocol, network, accountAddress).
		PageSize(limit).
		PageToken(continuation)

	fmt.Printf("Executing the request with: %s - %s, account address: %s and continuation: %s \n", protocol, network, accountAddress, continuation)
	txOutputs, resp, err := req.Execute()
	if err != nil {
		panic(
			fmt.Errorf("failed to paginate %s transactions: got status '%s' and error '%v'",
				protocol,
				resp.Status,
				err),
		)
	}

	continuation = getContinuation(txOutputs)
	fmt.Printf("Got %s transaction output page with size %d and continuation %s \n", protocol, txOutputs.GetTotal(), continuation)

	printTxsPage(txOutputs)

	return txOutputs, continuation, err
}

func getContinuation(txOutputs ubiquity.TxOutputs) string {
	if txOutputs.HasMeta() {
		return txOutputs.GetMeta().Paging.GetNextPageToken()
	}

	return ""
}

func printTxsPage(page ubiquity.TxOutputs) {
	for _, txOutput := range page.GetData() {
		examples.PrintTxOutput(txOutput)
	}
}
