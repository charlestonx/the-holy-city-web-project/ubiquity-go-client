module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples/platforms-overview

go 1.19

require gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0

require (
	github.com/golang/protobuf v1.4.3 // indirect
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)

replace gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client v0.0.0 => ../../
