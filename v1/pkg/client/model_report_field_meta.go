/*
 * Universal REST API
 *
 * Universal API provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple protocols/cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Currently supported protocols:  * algorand   * mainnet * bitcoin   * mainnet/testnet * bitcoincash   * mainnet/testnet * dogecoin   * mainnet/testnet * ethereum   * mainnet/goerli * litecoin   * mainnet/testnet * near   * mainnet/testnet * oasis   * mainnet * optimism   * mainnet * polkadot   * mainnet/westend * polygon   * mainnet * solana   * mainnet/testnet * stellar   * mainnet/testnet * tezos   * mainnet * xrp   * mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.
 *
 * API version: 3.0.0
 * Contact: support@blockdaemon.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package ubiquity

import (
	"encoding/json"
	"fmt"
)

// ReportFieldMeta - Additional metadata bespoke to specific protocols
type ReportFieldMeta struct {
	AlgorandMeta *AlgorandMeta
}

// AlgorandMetaAsReportFieldMeta is a convenience function that returns AlgorandMeta wrapped in ReportFieldMeta
func AlgorandMetaAsReportFieldMeta(v *AlgorandMeta) ReportFieldMeta {
	return ReportFieldMeta{AlgorandMeta: v}
}

// Unmarshal JSON data into one of the pointers in the struct
func (dst *ReportFieldMeta) UnmarshalJSON(data []byte) error {
	var err error
	// use discriminator value to speed up the lookup
	var jsonDict map[string]interface{}
	err = json.Unmarshal(data, &jsonDict)
	if err != nil {
		return fmt.Errorf("Failed to unmarshal JSON into map for the discrimintor lookup.")
	}

	// check if the discriminator value is 'algorand_meta'
	if jsonDict["type"] == "algorand_meta" {
		// try to unmarshal JSON data into AlgorandMeta
		err = json.Unmarshal(data, &dst.AlgorandMeta)
		if err == nil {
			return nil // data stored in dst.AlgorandMeta, return on the first match
		} else {
			dst.AlgorandMeta = nil
			return fmt.Errorf("Failed to unmarshal ReportFieldMeta as AlgorandMeta: %s", err.Error())
		}
	}

	return nil
}

// Marshal data from the first non-nil pointers in the struct to JSON
func (src ReportFieldMeta) MarshalJSON() ([]byte, error) {
	if src.AlgorandMeta != nil {
		return json.Marshal(&src.AlgorandMeta)
	}

	return nil, nil // no data in oneOf schemas
}

// Get the actual instance
func (obj *ReportFieldMeta) GetActualInstance() interface{} {
	if obj.AlgorandMeta != nil {
		return obj.AlgorandMeta
	}

	// all schemas are nil
	return nil
}

type NullableReportFieldMeta struct {
	value *ReportFieldMeta
	isSet bool
}

func (v NullableReportFieldMeta) Get() *ReportFieldMeta {
	return v.value
}

func (v *NullableReportFieldMeta) Set(val *ReportFieldMeta) {
	v.value = val
	v.isSet = true
}

func (v NullableReportFieldMeta) IsSet() bool {
	return v.isSet
}

func (v *NullableReportFieldMeta) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableReportFieldMeta(val *ReportFieldMeta) *NullableReportFieldMeta {
	return &NullableReportFieldMeta{value: val, isSet: true}
}

func (v NullableReportFieldMeta) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableReportFieldMeta) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
