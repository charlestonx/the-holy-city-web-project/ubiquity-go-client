/*
 * Universal REST API
 *
 * Universal API provides a RESTful and uniform way to access blockchain resources, with a rich and reusable model across multiple protocols/cryptocurrencies.  [Documentation](https://app.blockdaemon.com/docs/ubiquity)  ### Currently supported protocols:  * algorand   * mainnet * bitcoin   * mainnet/testnet * bitcoincash   * mainnet/testnet * dogecoin   * mainnet/testnet * ethereum   * mainnet/goerli * litecoin   * mainnet/testnet * near   * mainnet/testnet * oasis   * mainnet * optimism   * mainnet * polkadot   * mainnet/westend * polygon   * mainnet * solana   * mainnet/testnet * stellar   * mainnet/testnet * tezos   * mainnet * xrp   * mainnet  ##### Pagination Certain resources contain a lot of data, more than what's practical to return for a single request. With the help of pagination, the data is split across multiple responses. Each response returns a subset of the items requested, and a continuation token.  To get the next batch of items, copy the returned continuation token to the continuation query parameter and repeat the request with the new URL. In case no continuation token is returned, there is no more data available.
 *
 * API version: 3.0.0
 * Contact: support@blockdaemon.com
 */

// Code generated by OpenAPI Generator (https://openapi-generator.tech); DO NOT EDIT.

package ubiquity

import (
	"encoding/json"
)

// FeeEstimateEstimatedFees Object containing fast, medium, slow fees
type FeeEstimateEstimatedFees struct {
	Fast   interface{} `json:"fast,omitempty"`
	Medium interface{} `json:"medium,omitempty"`
	Slow   interface{} `json:"slow,omitempty"`
}

// NewFeeEstimateEstimatedFees instantiates a new FeeEstimateEstimatedFees object
// This constructor will assign default values to properties that have it defined,
// and makes sure properties required by API are set, but the set of arguments
// will change when the set of required properties is changed
func NewFeeEstimateEstimatedFees() *FeeEstimateEstimatedFees {
	this := FeeEstimateEstimatedFees{}
	return &this
}

// NewFeeEstimateEstimatedFeesWithDefaults instantiates a new FeeEstimateEstimatedFees object
// This constructor will only assign default values to properties that have it defined,
// but it doesn't guarantee that properties required by API are set
func NewFeeEstimateEstimatedFeesWithDefaults() *FeeEstimateEstimatedFees {
	this := FeeEstimateEstimatedFees{}
	return &this
}

// GetFast returns the Fast field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FeeEstimateEstimatedFees) GetFast() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Fast
}

// GetFastOk returns a tuple with the Fast field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FeeEstimateEstimatedFees) GetFastOk() (*interface{}, bool) {
	if o == nil || o.Fast == nil {
		return nil, false
	}
	return &o.Fast, true
}

// HasFast returns a boolean if a field has been set.
func (o *FeeEstimateEstimatedFees) HasFast() bool {
	if o != nil && o.Fast != nil {
		return true
	}

	return false
}

// SetFast gets a reference to the given interface{} and assigns it to the Fast field.
func (o *FeeEstimateEstimatedFees) SetFast(v interface{}) {
	o.Fast = v
}

// GetMedium returns the Medium field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FeeEstimateEstimatedFees) GetMedium() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Medium
}

// GetMediumOk returns a tuple with the Medium field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FeeEstimateEstimatedFees) GetMediumOk() (*interface{}, bool) {
	if o == nil || o.Medium == nil {
		return nil, false
	}
	return &o.Medium, true
}

// HasMedium returns a boolean if a field has been set.
func (o *FeeEstimateEstimatedFees) HasMedium() bool {
	if o != nil && o.Medium != nil {
		return true
	}

	return false
}

// SetMedium gets a reference to the given interface{} and assigns it to the Medium field.
func (o *FeeEstimateEstimatedFees) SetMedium(v interface{}) {
	o.Medium = v
}

// GetSlow returns the Slow field value if set, zero value otherwise (both if not set or set to explicit null).
func (o *FeeEstimateEstimatedFees) GetSlow() interface{} {
	if o == nil {
		var ret interface{}
		return ret
	}
	return o.Slow
}

// GetSlowOk returns a tuple with the Slow field value if set, nil otherwise
// and a boolean to check if the value has been set.
// NOTE: If the value is an explicit nil, `nil, true` will be returned
func (o *FeeEstimateEstimatedFees) GetSlowOk() (*interface{}, bool) {
	if o == nil || o.Slow == nil {
		return nil, false
	}
	return &o.Slow, true
}

// HasSlow returns a boolean if a field has been set.
func (o *FeeEstimateEstimatedFees) HasSlow() bool {
	if o != nil && o.Slow != nil {
		return true
	}

	return false
}

// SetSlow gets a reference to the given interface{} and assigns it to the Slow field.
func (o *FeeEstimateEstimatedFees) SetSlow(v interface{}) {
	o.Slow = v
}

func (o FeeEstimateEstimatedFees) MarshalJSON() ([]byte, error) {
	toSerialize := map[string]interface{}{}
	if o.Fast != nil {
		toSerialize["fast"] = o.Fast
	}
	if o.Medium != nil {
		toSerialize["medium"] = o.Medium
	}
	if o.Slow != nil {
		toSerialize["slow"] = o.Slow
	}
	return json.Marshal(toSerialize)
}

type NullableFeeEstimateEstimatedFees struct {
	value *FeeEstimateEstimatedFees
	isSet bool
}

func (v NullableFeeEstimateEstimatedFees) Get() *FeeEstimateEstimatedFees {
	return v.value
}

func (v *NullableFeeEstimateEstimatedFees) Set(val *FeeEstimateEstimatedFees) {
	v.value = val
	v.isSet = true
}

func (v NullableFeeEstimateEstimatedFees) IsSet() bool {
	return v.isSet
}

func (v *NullableFeeEstimateEstimatedFees) Unset() {
	v.value = nil
	v.isSet = false
}

func NewNullableFeeEstimateEstimatedFees(val *FeeEstimateEstimatedFees) *NullableFeeEstimateEstimatedFees {
	return &NullableFeeEstimateEstimatedFees{value: val, isSet: true}
}

func (v NullableFeeEstimateEstimatedFees) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.value)
}

func (v *NullableFeeEstimateEstimatedFees) UnmarshalJSON(src []byte) error {
	v.isSet = true
	return json.Unmarshal(src, &v.value)
}
