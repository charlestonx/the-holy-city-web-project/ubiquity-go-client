package ubiquityws

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const (
	wsURLPattern = "wss://svc.blockdaemon.com/universal/v1/%s/%s/websocket"

	blockIDChannel = "ubiquity.block_identifiers"
	txChannel      = "ubiquity.txs"

	subscriptionMethod = "ubiquity.subscription"

	defaultPublishTimeoutSec  = 60
	defaultResponseTimeoutSec = 60
)

// Client implements Ubiquity Websocket API - https://app.blockdaemon.com/docs/ubiquity#ubiquity-web-sockets-api
type Client struct {
	logger             *wsLogger
	wsConn             *websocket.Conn
	reqIDCounter       int64
	rMtx               sync.Mutex // Mutex for WS requests, only one write allowed at a time
	pendingRequests    sync.Map
	publishTimeoutSec  int64
	responseTimeoutSec int64
	publishTimeout     time.Duration
	responseTimeout    time.Duration
	sMtx               sync.RWMutex // Mutex to protect the below subscribers
	blockIDSubscribers map[string]chan *BlockIdentifier
	txSubscribers      map[string]chan *Tx
}

type Config struct {
	// You should specify Platform (one of ubiquitypl.*) or WebsocketURL along with APIKey
	Platform           string
	Network            string // mainnet by default
	WebsocketURL       string // You can use one of the constants ubiquityws.Endpoint*
	APIKey             string
	PublishTimeoutSec  int64
	ResponseTimeoutSec int64
	ShowDebug          bool
}

func NewClient(conf *Config) (*Client, error) {
	var wsURL string
	network := "mainnet"
	if conf.Network != "" {
		network = conf.Network
	}

	if pl := conf.Platform; pl != "" {
		wsURL = fmt.Sprintf(wsURLPattern, pl, network)
	} else if conf.WebsocketURL != "" {
		wsURL = conf.WebsocketURL
	} else {
		return nil, errors.New("either Platform or WebsocketURL must be provided")
	}

	if conf.APIKey == "" {
		return nil, errors.New("API key must be provided")
	}
	authHeader := http.Header{"Authorization": []string{"Bearer " + conf.APIKey}}

	conn, resp, err := websocket.DefaultDialer.Dial(wsURL, authHeader)
	if err != nil {
		return nil, fmt.Errorf("failed to connect: err = %v and resp = %v", err, resp)
	}

	publishTimeout := conf.PublishTimeoutSec
	if publishTimeout <= 0 {
		publishTimeout = defaultPublishTimeoutSec
	}
	responseTimeout := conf.ResponseTimeoutSec
	if responseTimeout <= 0 {
		responseTimeout = defaultResponseTimeoutSec
	}

	c := &Client{
		logger: &wsLogger{
			Logger:    log.New(os.Stdout, "ubiquityws.logger ", log.LstdFlags),
			showDebug: conf.ShowDebug,
		},
		wsConn:             conn,
		publishTimeoutSec:  publishTimeout,
		responseTimeoutSec: responseTimeout,
		publishTimeout:     time.Duration(publishTimeout) * time.Second,
		responseTimeout:    time.Duration(responseTimeout) * time.Second,
		blockIDSubscribers: make(map[string]chan *BlockIdentifier),
		txSubscribers:      make(map[string]chan *Tx),
	}
	go c.runWSMessageProcessing()

	return c, nil
}

type newMessage struct {
	Id     int64                  `json:"id"`
	Result map[string]interface{} `json:"result"`
	Error  map[string]interface{} `json:"error"`
	Method string                 `json:"method"`
	Params *wsNotificationParams  `json:"params"`
}

func (c *Client) runWSMessageProcessing() {

	for {
		msg := newMessage{}

		if err := c.wsConn.ReadJSON(&msg); err != nil {
			if strings.Contains(err.Error(), "use of closed network connection") {
				c.logger.info("WS message processing terminated: connection closed") // WS client was closed
				return
			}

			c.logger.error("WS message processing terminated due to read error: %v", err)
			go func() {
				c.logger.info("Closing a client...")
				if err := c.Close(); err != nil {
					c.logger.warn("failed to close: %v", err)
				}
			}()

			return
		}

		if err := c.processWSMessage(msg); err != nil {
			c.logger.error("Got error '%v' while processing WS message: %v", err, msg)
		}
	}
}

func (c *Client) processWSMessage(msg newMessage) error {
	c.logger.debug("Received WS message: %v", msg.Method)

	switch {
	case msg.Id > 0:
		return c.handleSubscribeMessage(&msg)
	case msg.Method != "":
		if msg.Method != subscriptionMethod {
			return fmt.Errorf("unsupported wsNotification method: %s", msg.Method)
		}

		c.sMtx.RLock()
		defer c.sMtx.RUnlock()

		for _, item := range msg.Params.Items {
			err := c.sendMessageToCh(item)
			if err != nil {
				return err
			}
		}

	default:
		return errors.New("unsupported JSON structure")
	}

	return nil
}

func (c *Client) sendMessageToCh(item *wsNotificationParamsItem) error {
	subIDStr := strconv.FormatInt(item.SubID, 10)
	var content interface{}

	switch item.Channel {
	case blockIDChannel:
		content = new(ubiquity.BlockIdentifier)
		err := json.Unmarshal(item.Content, &content)
		if err != nil {
			return err
		}

		blockIdentifierCh, ok := c.blockIDSubscribers[subIDStr]
		if !ok {
			return fmt.Errorf("no '%s' subscriber for subID #%s", blockIDChannel, subIDStr)
		}

		select {
		case blockIdentifierCh <- &BlockIdentifier{
			BlockIdentifier: content.(*ubiquity.BlockIdentifier),
			Revert:          item.Revert,
		}:
		case <-time.After(c.publishTimeout):
			return timeOutError(subIDStr)
		}

	case txChannel:
		content = new(ubiquity.Tx)
		_ = json.Unmarshal(item.Content, &content)
		txCh, ok := c.txSubscribers[subIDStr]
		if !ok {
			return fmt.Errorf("no '%s' subscriber for subID #%s", txChannel, subIDStr)
		}

		select {
		case txCh <- &Tx{
			Tx:     content.(*ubiquity.Tx),
			Revert: item.Revert,
		}:
		case <-time.After(c.publishTimeout):
			return timeOutError(subIDStr)
		}
	default:
		return fmt.Errorf("unsupported channel type: %s", item.Channel)
	}

	return nil
}

func timeOutError(subID string) error {
	return fmt.Errorf("failed while processing the notification items: incomming message for subID #%s"+
		" will be dropped:timed out while waiting for it to be consumed", subID)
}

func (c *Client) handleSubscribeMessage(msg *newMessage) error {

	reqIDStr := strconv.FormatInt(msg.Id, 10)
	pendingCh, ok := c.pendingRequests.Load(reqIDStr)
	if !ok {
		return fmt.Errorf("unknown Request ID: %s", reqIDStr)
	}

	respCh := pendingCh.(chan *newMessage)
	defer close(respCh)

	select {
	case respCh <- msg:
		return nil
	case <-time.After(c.responseTimeout):
		return fmt.Errorf("WS response for req. #%s will be dropped: timed out while "+
			"waiting for it to be consumed", reqIDStr)
	}

}

// SubscribeBlockIDs subscribes a channel "ubiquity.block_identifiers".
// It returns a notification channel with subID or error.
func (c *Client) SubscribeBlockIDs() (string, <-chan *BlockIdentifier, error) {
	subID, err := c.subscribe(blockIDChannel, make(map[string]interface{}))
	if err != nil {
		return "", nil, fmt.Errorf("failed to subscribe '%s': %v", blockIDChannel, err)
	}

	subC := make(chan *BlockIdentifier)

	c.sMtx.Lock()
	c.blockIDSubscribers[subID] = subC
	c.sMtx.Unlock()

	return subID, subC, nil
}

// UnsubscribeBlockIDs unsubscribes a channel "ubiquity.block_identifiers".
func (c *Client) UnsubscribeBlockIDs(subID string) error {
	c.sMtx.Lock()
	s := c.blockIDSubscribers[subID]
	if s != nil {
		delete(c.blockIDSubscribers, subID)
		close(s)
	} else {
		c.logger.warn("Couldn't find '%s' subscriber for subID #%s", subID, blockIDChannel)
	}
	c.sMtx.Unlock()

	if err := c.unsubscribe(subID, blockIDChannel); err != nil {
		return fmt.Errorf("failed to unsubscribe '%s': %v", blockIDChannel, err)
	}

	return nil
}

type TxsFilter struct {
	Assets    []string
	Addresses []string
}

// SubscribeTxs subscribes a channel "ubiquity.txs" with optionally provided filtering over assets and addresses.
// It returns a notification channel with subID or error.
func (c *Client) SubscribeTxs(filter *TxsFilter) (string, <-chan *Tx, error) {
	if filter == nil {
		filter = &TxsFilter{}
	}

	subID, err := c.subscribe(txChannel, map[string]interface{}{
		"addresses": filter.Addresses,
		"assets":    filter.Assets,
	})
	if err != nil {
		return "", nil, fmt.Errorf("failed to subscribe '%s': %v", txChannel, err)
	}

	subC := make(chan *Tx)

	c.sMtx.Lock()
	c.txSubscribers[subID] = subC
	c.sMtx.Unlock()

	return subID, subC, nil
}

// UnsubscribeTxs unsubscribes a channel "ubiquity.txs"
func (c *Client) UnsubscribeTxs(subID string) error {
	c.sMtx.Lock()
	s := c.txSubscribers[subID]
	if s != nil {
		delete(c.txSubscribers, subID)
		close(s)
	} else {
		c.logger.warn("Couldn't find '%s' subscriber for subID #%s", subID, txChannel)
	}
	c.sMtx.Unlock()

	if err := c.unsubscribe(subID, txChannel); err != nil {
		return fmt.Errorf("failed to unsubscribe '%s': %v", txChannel, err)
	}

	return nil
}

func (c *Client) subscribe(channel string, details map[string]interface{}) (string, error) {
	reqID := c.nextRequestID()
	resp, err := c.sendWSRequest(newSubscribeRequest(reqID, channel, details))
	if err != nil || resp.Error != nil {
		return "", fmt.Errorf("failed to subscribe: err = %v and response = %v", err, resp)
	}

	subID, ok := (resp.Result)["subID"].(float64)
	if !ok {
		return "", fmt.Errorf("failed to convert 'subID' into float64")
	}

	subIDStr := strconv.FormatInt(int64(subID), 10)
	return subIDStr, nil
}

func (c *Client) unsubscribe(subID, channel string) error {
	reqID := c.nextRequestID()
	subIDNum, err := strconv.ParseInt(subID, 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert subID %s: %v", subID, err)
	}
	resp, err := c.sendWSRequest(newUnsubscribeRequest(reqID, channel, subIDNum))
	if err != nil || resp.Error != nil {
		return fmt.Errorf("failed to unsubscribe: err = %v and response = %v", err, resp)
	}

	removed, ok := (resp.Result)["removed"].(bool)
	if !ok {
		return fmt.Errorf("failed to convert 'removed' into bool")
	}
	if !removed {
		c.logger.warn("Couldn't remove subscription with ID '%s'", subID)
	}
	return nil
}

func (c *Client) nextRequestID() int64 {
	c.rMtx.Lock()
	defer c.rMtx.Unlock()

	c.reqIDCounter++
	return c.reqIDCounter
}

func (c *Client) sendWSRequest(req *wsRequest) (*newMessage, error) {
	c.rMtx.Lock()
	defer c.rMtx.Unlock()

	err := c.wsConn.WriteJSON(req)
	if err != nil {
		return nil, fmt.Errorf("websocket request failure: %v", err)
	}

	respChan := make(chan *newMessage)
	reqIDStr := strconv.FormatInt(req.ID, 10)

	c.pendingRequests.Store(reqIDStr, respChan)
	defer c.pendingRequests.Delete(reqIDStr)

	select {
	case resp := <-respChan:
		if resp == nil {
			return nil, errors.New("received nil response for req. #" + reqIDStr)
		}
		return resp, nil

	case <-time.After(c.responseTimeout):
		return nil, errors.New("timed out while waiting for request #" + reqIDStr + " response")
	}
}

// Close closes WS client and releases the resources
func (c *Client) Close() error {
	c.unsubscribeAll()
	err := c.wsConn.Close()
	if err != nil {
		return fmt.Errorf("failed to close WS conn: %v", err)
	}
	return nil
}

func (c *Client) unsubscribeAll() {
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		for subID := range c.blockIDSubscribers {
			if err := c.UnsubscribeBlockIDs(subID); err != nil {
				c.logger.error("failed to unsubscribe: %v", err)
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for subID := range c.txSubscribers {
			if err := c.UnsubscribeTxs(subID); err != nil {
				c.logger.error("failed to unsubscribe: %v", err)
			}
		}
	}()

	wg.Wait()
}
