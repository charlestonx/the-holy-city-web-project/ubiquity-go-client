package ubiquitytx

import (
	"context"
	"fmt"
	"math/big"
	"strings"

	"github.com/centrifuge/go-substrate-rpc-client/v4/signature"
	"github.com/centrifuge/go-substrate-rpc-client/v4/types"
	"github.com/centrifuge/go-substrate-rpc-client/v4/types/codec"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

type PolkadotTransaction struct {
	Network    string
	To         string
	Amount     *big.Int
	Tip        uint64
	SeedPhrase string
}

func (t PolkadotTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if strings.TrimSpace(t.SeedPhrase) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	if strings.TrimSpace(t.To) == "" {
		return fmt.Errorf("field 'To' is required")
	}
	if t.Amount == nil {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

type PolkadotNetwork uint8

const (
	PolkadotMainnet PolkadotNetwork = 0
	PolkadotWestend PolkadotNetwork = 42
)

// SendDOT creates, signs and sends DOT transaction. Under the hood it uses Ubiquity TxCreate and TxSend API.
func (s UbiquityTransactionService) SendDOT(ctx context.Context, tx *PolkadotTransaction) (*SendTxResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("polkadot transaction validation failure: %v", err)
	}

	var networkId PolkadotNetwork
	switch tx.Network {
	case "mainnet":
		networkId = PolkadotMainnet
	case "westend":
		networkId = PolkadotWestend
	default:
		panic(fmt.Errorf("network %s not supported! Only mainnet and westend are supported", tx.Network))
	}

	keyPair, err := signature.KeyringPairFromSecret(tx.SeedPhrase, uint8(networkId))
	if err != nil {
		return nil, fmt.Errorf("error when obtaining key pair from seed phrase: %w", err)
	}

	from := keyPair.Address
	tipStr := fmt.Sprintf("%d", tx.Tip)

	txRes, resp, err := s.txAPI.
		TxCreate(ctx, "polkadot", tx.Network).
		TxCreate(ubiquity.TxCreate{
			From: from,
			To: []ubiquity.TxDestination{{
				Destination: tx.To,
				Amount:      tx.Amount.String(),
			}},
			Fee: &tipStr,
		}).
		Execute()

	if err != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	rawTx := txRes.UnsignedTx

	if txRes.Meta == nil {
		return nil, fmt.Errorf("couldn't find 'signing_payload' key in TxCreate response's meta object")
	}

	signingPayloadInterface, ok := (*txRes.Meta)["signing_payload"]
	if !ok {
		return nil, fmt.Errorf("couldn't find 'signing_payload' key in TxCreate response's meta object")
	}

	signingPayload, ok := signingPayloadInterface.(string)
	if !ok {
		return nil, fmt.Errorf("'signing_payload' key in TxCreate response's meta object is not a string")
	}

	rawSignedTx, err := SignPolkadotTransaction(tx.SeedPhrase, rawTx, signingPayload, networkId)
	if err != nil {
		return nil, fmt.Errorf("error signing transaction")
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "polkadot", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: rawSignedTx}).Execute()
	if err != nil {
		return nil, fmt.Errorf("ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendTxResult{
		TxHash: txReceipt.Id,
		TxID:   txReceipt.GetId(),
	}, nil
}

func SignPolkadotTransaction(seedPhrase, rawTx, rawSigningPayload string, network PolkadotNetwork) (string, error) {
	lenSigningPayload := len(rawSigningPayload)
	// get block hash from signing payload - it is in the last 32 bytes (64 chars of a hex string)
	blockHash, err := types.NewHashFromHexString("0x" + rawSigningPayload[lenSigningPayload-64:])
	if err != nil {
		return "", fmt.Errorf("error when decoding payload's block hash: %w", err)
	}

	// get genesis block hash from signing payload - it is in the next 32 bytes (64 chars of a hex string) from the block hash
	genesisHash, err := types.NewHashFromHexString("0x" + rawSigningPayload[lenSigningPayload-(64*2):lenSigningPayload-64])
	if err != nil {
		return "", fmt.Errorf("error when decoding payload's genesis hash: %w", err)
	}

	// decode the raw tx string into a types.Extrinsic object
	var ext types.Extrinsic
	if err := codec.DecodeFromHex(rawTx, &ext); err != nil {
		return "", fmt.Errorf("error when decoding raw transaction: %w", err)
	}

	keyPair, err := signature.KeyringPairFromSecret(seedPhrase, uint8(network))
	if err != nil {
		return "", fmt.Errorf("error when obtaining key pair from seed phrase: %w", err)
	}

	nonce := ext.Signature.Nonce
	era := ext.Signature.Era

	// get spec and transaction version from signing payload - they are stored in the next 16 bytes (32 chars of a hex string)
	//  from the genesis hash
	var transactionVersion, specVersion types.U32
	transactionVersionHex := "0x" + rawSigningPayload[lenSigningPayload-((64*2)+8):lenSigningPayload-(64*2)]

	if err := codec.DecodeFromHex(transactionVersionHex, &transactionVersion); err != nil {
		return "", fmt.Errorf("error decoding the payload's transaction version")
	}

	specVersionHex := "0x" + rawSigningPayload[lenSigningPayload-((64*2)+2*8):lenSigningPayload-((64*2)+8)]
	if err := codec.DecodeFromHex(specVersionHex, &specVersion); err != nil {
		return "", fmt.Errorf("error decoding the payload's spec version")
	}

	o := types.SignatureOptions{
		BlockHash:          blockHash,
		Era:                era,
		GenesisHash:        genesisHash,
		Nonce:              nonce,
		SpecVersion:        specVersion,
		Tip:                ext.Signature.Tip,
		TransactionVersion: transactionVersion,
	}

	err = ext.Sign(keyPair, o)
	if err != nil {
		return "", fmt.Errorf("error when signing raw transaction: %w", err)
	}

	rawSignedExt, err := codec.EncodeToHex(ext)
	if err != nil {
		return "", fmt.Errorf("error when encoding signed transaction: %w", err)
	}

	return rawSignedExt, nil
}
