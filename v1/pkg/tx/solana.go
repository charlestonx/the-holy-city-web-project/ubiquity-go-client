package ubiquitytx

import (
	"context"
	"fmt"

	"github.com/gagliardetto/solana-go"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

type SolanaTransaction struct {
	Network        string
	From           solana.PrivateKey
	To             string
	AmountLamports int64
}

func (t SolanaTransaction) validate() error {
	if t.Network != networkMainnet && t.Network != networkTestnet {
		return fmt.Errorf("field 'Network' should be 'mainnet' or 'testnet'")
	}
	if _, err := solana.PublicKeyFromBase58(t.To); err != nil {
		return fmt.Errorf("field 'To' is an invalid Solana address")
	}
	if t.AmountLamports == 0 {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

// SendSOL creates, signs and sends SOL transaction. Under the hood it uses Ubiquity TxCreate and TxSend API.
func (s UbiquityTransactionService) SendSOL(ctx context.Context, tx *SolanaTransaction) (*SendTxResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("solana transaction validation failure: %v", err)
	}

	txRes, resp, err := s.txAPI.
		TxCreate(ctx, "solana", tx.Network).
		TxCreate(ubiquity.TxCreate{
			From: tx.From.PublicKey().String(),
			To: []ubiquity.TxDestination{{
				Destination: tx.To,
				Amount:      fmt.Sprintf("%d", tx.AmountLamports),
			}},
		}).
		Execute()

	if err != nil && resp != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	} else if err != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: %w", err)
	}

	signedRawTx, err := SignSolanaTransaction(txRes.UnsignedTx, tx.From)
	if err != nil {
		return nil, fmt.Errorf("failed to sign Solana transaction: %w", err)
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "solana", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: signedRawTx}).Execute()
	if err != nil {
		return nil, fmt.Errorf("ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendTxResult{
		TxHash: txReceipt.Id,
		TxID:   txReceipt.GetId(),
	}, nil
}

func SignSolanaTransaction(unsignedRawTx string, privateKey solana.PrivateKey) (signedRawTx string, err error) {
	privateKeyGetter := func(key solana.PublicKey) *solana.PrivateKey {
		if key == privateKey.PublicKey() {
			return &privateKey
		}
		return nil
	}

	solTx := new(solana.Transaction)
	if err := solTx.UnmarshalBase64(unsignedRawTx); err != nil {
		return "", err
	}

	if _, err := solTx.Sign(privateKeyGetter); err != nil {
		return "", err
	}

	signedRawTx, err = solTx.ToBase64()
	return
}
