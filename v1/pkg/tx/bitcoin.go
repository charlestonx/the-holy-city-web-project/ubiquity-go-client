package ubiquitytx

import (
	"bytes"
	"context"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

// BitcoinTransaction *Important* make sure to properly calculate the difference between INs and OUTs
// since it will be automatically payed as a Fee.
// In the future we may include UTXO and Fee calculation into the service.
type BitcoinTransaction struct {
	Network string
	From    []TxInput
	To      []TxOutput
	// Fee        int64
	// ChangeAddr string
	PrivateKey string
}

// Struct validation wasn't used in order to eliminate extra dependencies for SDK users.
func (t BitcoinTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if len(t.From) == 0 {
		return fmt.Errorf("field 'From' must not be empty")
	}
	for _, in := range t.From {
		if in.Source == "" {
			return fmt.Errorf("input fields 'Source' and 'Index' must be set")
		}
	}
	if len(t.To) == 0 {
		return fmt.Errorf("field 'To' must not be empty")
	}
	for _, out := range t.To {
		if out.Destination == "" {
			return fmt.Errorf("output fields 'Destination' and 'Amount' must be set")
		}
	}
	if strings.TrimSpace(t.PrivateKey) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	return nil
}

// SendBTC creates, signs and sends BTC transaction. Under the hood it uses Ubiquity TxSend API.
func (s UbiquityTransactionService) SendBTC(ctx context.Context, tx *BitcoinTransaction) (*SendTxResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("bitcoin transaction validation failure: %v", err)
	}

	msgTx := wire.NewMsgTx(wire.TxVersion)

	for _, in := range tx.From {
		txHash, err := chainhash.NewHashFromStr(in.Source)
		if err != nil {
			return nil, fmt.Errorf("failed to get hash from source string: %v", err)
		}
		msgTx.AddTxIn(wire.NewTxIn(wire.NewOutPoint(txHash, in.Index), nil, nil))
	}

	netParams := &chaincfg.MainNetParams
	if tx.Network == networkTestnet {
		netParams = &chaincfg.TestNet3Params
	}
	for _, out := range tx.To {
		destAddr, err := btcutil.DecodeAddress(out.Destination, netParams)
		if err != nil {
			return nil, fmt.Errorf("failed to decode destination address: %v", err)
		}
		pkScript, err := txscript.PayToAddrScript(destAddr)
		if err != nil {
			return nil, fmt.Errorf("failed to build P2PKH script: %v", err)
		}

		msgTx.AddTxOut(wire.NewTxOut(out.Amount, pkScript))
	}

	signedTx, err := signBitcoinTx(tx.PrivateKey, msgTx, netParams)
	if err != nil {
		return nil, fmt.Errorf("failed to sign Bitcoin transaction: %v", err)
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "bitcoin", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: signedTx}).Execute()
	if err != nil {
		return nil, fmt.Errorf("ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendTxResult{
		TxHash: msgTx.TxHash().String(),
		TxID:   txReceipt.GetId(),
	}, nil
}

func signBitcoinTx(privKey string, msgTx *wire.MsgTx, netParams *chaincfg.Params) (string, error) {
	wif, err := btcutil.DecodeWIF(privKey)
	if err != nil {
		return "", fmt.Errorf("failed to decode WIF: %v", err)
	}
	pubKey, err := btcutil.NewAddressPubKey(wif.PrivKey.PubKey().SerializeUncompressed(), netParams)
	if err != nil {
		return "", fmt.Errorf("failed to build pubKey: %v", err)
	}
	pubKeyAddress, err := btcutil.DecodeAddress(pubKey.EncodeAddress(), netParams)
	if err != nil {
		return "", fmt.Errorf("failed to decode address: %v", err)
	}
	inputPKScript, err := txscript.PayToAddrScript(pubKeyAddress)
	if err != nil {
		return "", fmt.Errorf("failed to build P2PKH script: %v", err)
	}

	for i, in := range msgTx.TxIn {
		sigScript, err := txscript.SignatureScript(msgTx, i, inputPKScript, txscript.SigHashAll, wif.PrivKey, false)
		if err != nil {
			return "", fmt.Errorf("failed to sign: %v", err)
		}
		in.SignatureScript = sigScript
	}

	var signedTx bytes.Buffer
	_ = msgTx.Serialize(&signedTx)
	encodedSignedTx := hex.EncodeToString(signedTx.Bytes())

	return encodedSignedTx, nil
}
