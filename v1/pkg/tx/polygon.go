package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

var (
	polygonChains = map[string]*big.Int{
		networkMainnet: big.NewInt(137),
		networkTestnet: big.NewInt(80001),
	}
)

type PolygonTransaction struct {
	Network              string
	PrivateKey           string
	To                   string
	Amount               *big.Float // In Matic
	Nonce                *int64     // Current transaction count
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int
	Gas                  *big.Int
}

func (t PolygonTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if strings.TrimSpace(t.PrivateKey) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	if strings.TrimSpace(t.To) == "" {
		return fmt.Errorf("field 'To' is required")
	}
	if t.Amount == nil {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

// SendMATIC creates, signs and sends MATIC transaction. Under the hood it uses Ubiquity TxCreate and TxSend API.
func (s UbiquityTransactionService) SendMATIC(ctx context.Context, tx *PolygonTransaction) (*SendTxResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("polygon transaction validation failure: %v", err)
	}

	privKey, err := crypto.HexToECDSA(tx.PrivateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %v", err)
	}

	pubKey, ok := privKey.Public().(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("unexpected public key type: %T", pubKey)
	}

	from := crypto.PubkeyToAddress(*pubKey)
	var gas, maxFeePerGas, maxPriorityFeePerGas *int64
	if tx.Gas != nil {
		gas = toPointer(tx.Gas.Int64())
	}
	if tx.MaxFeePerGas != nil {
		maxFeePerGas = toPointer(tx.MaxFeePerGas.Int64())
	}

	if tx.MaxPriorityFeePerGas != nil {
		maxPriorityFeePerGas = toPointer(tx.MaxPriorityFeePerGas.Int64())
	}

	txRes, resp, err := s.txAPI.
		TxCreate(ctx, "polygon", tx.Network).
		TxCreate(ubiquity.TxCreate{
			From: from.Hex(),
			To: []ubiquity.TxDestination{{
				Destination: tx.To,
				Amount:      tx.Amount.String(),
			}},
			Index: tx.Nonce,
			Protocol: &ubiquity.TxCreateProtocol{
				Polygon: &ubiquity.TxCreateEvm{
					Gas:                  gas,
					MaxPriorityFeePerGas: maxPriorityFeePerGas,
					MaxFeePerGas:         maxFeePerGas,
				},
			},
		}).
		Execute()

	if err != nil && resp != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	} else if err != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: %w", err)
	}

	txHash, signedTxData, err := SignPolygonTransaction(txRes.UnsignedTx, tx.Network, privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "polygon", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: hex.EncodeToString(signedTxData)}).Execute()
	if err != nil {
		return nil, fmt.Errorf("ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendTxResult{
		TxHash: txHash,
		TxID:   txReceipt.GetId(),
	}, nil
}

// SignPolygonTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a post London hard fork transaction, als referred to as an EIP1559 transaction.
func SignPolygonTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := polygonChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewLondonSigner(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}
