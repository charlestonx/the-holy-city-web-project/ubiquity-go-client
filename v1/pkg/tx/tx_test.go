package ubiquitytx

import (
	"context"
	"fmt"
	"math/big"
	"net/http"
	"testing"

	testifyAssert "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/tx/mocks"
)

func TestBitcoinTransaction_validate(t *testing.T) {
	assert := testifyAssert.New(t)

	testCases := []BitcoinTransaction{
		{
			From:       []TxInput{{Source: "abc", Index: 0}},
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			From:       []TxInput{{Source: "abc", Index: 0}},
			PrivateKey: "<private key>",
		},
		{
			Network: "testnet",
			From:    []TxInput{{Source: "abc", Index: 0}},
			To:      []TxOutput{{Destination: "def", Amount: 12345}},
		},
		{
			Network:    "testnet",
			From:       []TxInput{{}},
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			From:       []TxInput{{Source: "abc", Index: 0}},
			To:         []TxOutput{{}},
			PrivateKey: "<private key>",
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			assert.Error(tc.validate())
		})
	}
}

func TestUbiquityTransactionService_SendBTCTestnet(t *testing.T) {
	assert := testifyAssert.New(t)

	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	network := "testnet"
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	txAPIMock.On("TxSend", ctx, "bitcoin", network).Return(txSendRequest)
	newTxID := "<new tx id>"
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)

	txService := NewService(txAPIMock, nil)

	r, err := txService.SendBTC(ctx, &BitcoinTransaction{
		Network: network,
		From:    []TxInput{{Source: "3c7be4102cefc284ef36237a09a3facaa16cec0ffa7eab36302dbf1294734e09", Index: 1}},
		To: []TxOutput{
			{Destination: "mqbGHFBYtsHo5rWoPdCgjoJcVPig96UWv8", Amount: 1000}, // Testnet addresses
			{Destination: "n17f2LuLKddqWj59a4wUc8Ar7iakcPdsEo", Amount: 8000},
		},
		PrivateKey: "P9Z49xp3dXnY5gvbjr2Wc3bDv2U7JovE75qbjJfGJFicek7tFAsX",
	})

	assert.NoError(err)
	assert.Equal(newTxID, r.TxID)
	assert.NotEmpty(r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestUbiquityTransactionService_SendBTCMainnet(t *testing.T) {
	assert := testifyAssert.New(t)

	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	network := "mainnet"
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	txAPIMock.On("TxSend", ctx, "bitcoin", network).Return(txSendRequest)
	newTxID := "<new tx id>"
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)

	txService := NewService(txAPIMock, nil)

	r, err := txService.SendBTC(ctx, &BitcoinTransaction{
		Network: network,
		From:    []TxInput{{Source: "782ad4d316fa75262bc70be944ca23a39c4b41e5f83368dc67d176dddef169ba", Index: 1}},
		To: []TxOutput{
			{Destination: "35DjpnyX3MySbHbCN4PYq9qnKbDq8sN4Ka", Amount: 1000}, // Mainnet addresses
			{Destination: "1Kr6QSydW9bFQG1mXiPNNu6WpJGmUa9i1g", Amount: 8000},
		},
		PrivateKey: "P9Z49xp3dXnY5gvbjr2Wc3bDv2U7JovE75qbjJfGJFicek7tFAsX", // We can use same private key
	})

	assert.NoError(err)
	assert.Equal(newTxID, r.TxID)
	assert.NotEmpty(r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestEthereumTransaction_validate(t *testing.T) {
	assert := testifyAssert.New(t)

	var nonce int64 = 1

	testCases := []EthereumTransaction{
		{
			PrivateKey: "<private key>",
			To:         "abc",
			Amount:     big.NewFloat(1.2345),
			Nonce:      &nonce,
		},
		{
			Network: "testnet",
			To:      "abc",
			Amount:  big.NewFloat(1.2345),
			Nonce:   &nonce,
		},
		{
			Network:    "testnet",
			PrivateKey: "<private key>",
			Amount:     big.NewFloat(1.2345),
			Nonce:      &nonce,
		},
		{
			Network:    "testnet",
			PrivateKey: "<private key>",
			To:         "abc",
			Nonce:      &nonce,
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			assert.Error(tc.validate())
		})
	}
}

func TestUbiquityTransactionService_SendETH(t *testing.T) {
	testCases := []struct {
		name, network                      string
		maxFeePerGas, maxPriorityFeePerGas *big.Int
		estimatedFee                       map[string]interface{}
	}{
		{
			name:                 "MainnetWithFees",
			network:              "mainnet",
			maxFeePerGas:         new(big.Int),
			maxPriorityFeePerGas: new(big.Int),
		},
		{
			name:    "MainnetWithoutFeesShouldEstimate",
			network: "mainnet",
			estimatedFee: map[string]interface{}{
				"max_priority_fee": 1234.0,
				"max_total_fee":    12345.0,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			testSendETH(t, tc.network, tc.maxFeePerGas, tc.maxPriorityFeePerGas, tc.estimatedFee)
		})
	}
}

func testSendETH(t *testing.T, network string, maxFeePerGas, maxPriorityFeePerGas *big.Int, estimatedFee map[string]interface{}) {
	assert := testifyAssert.New(t)

	txAPIMock := new(mocks.TransactionsAPI)
	feesAPIMock := new(mocks.GasEstimatorAPI)

	ctx := context.Background()
	newTxID := "<new tx id>"

	txCreateRequest := ubiquity.ApiTxCreateRequest{ApiService: txAPIMock}
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	feeEstimateRequest := ubiquity.ApiGetGasFeeEstimateRequest{ApiService: feesAPIMock}
	feesAPIMock.On("GetGasFeeEstimate", ctx, "ethereum", network).Return(feeEstimateRequest)
	feesAPIMock.On("GetGasFeeEstimateExecute", mock.AnythingOfType("ubiquity.ApiGetGasFeeEstimateRequest")).
		Return(ubiquity.FeeEstimate{EstimatedFees: &ubiquity.FeeEstimateEstimatedFees{Medium: estimatedFee}}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)
	txAPIMock.On("TxCreate", ctx, "ethereum", network).Return(txCreateRequest)
	txAPIMock.On("TxCreateExecute", mock.AnythingOfType("ubiquity.ApiTxCreateRequest")).
		Return(ubiquity.UnsignedTx{UnsignedTx: "02e880038203e88207d082520894e40873f0a7231c301e08e2703868721c10e955c28203e880c0808080"}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)
	txAPIMock.On("TxSend", ctx, "ethereum", network).Return(txSendRequest)
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)

	txService := NewService(txAPIMock, feesAPIMock)

	r, err := txService.SendETH(ctx, &EthereumTransaction{
		Network:              network,
		PrivateKey:           "56f8b9c2469e3d1fec30fa7e0623ca2c48e861bb9384977178c6a72bdde38cd0",
		To:                   "0x47128c68cF64a0aff7b436909e1Bd9A46168c93C",
		Amount:               big.NewFloat(0000.1),
		MaxFeePerGas:         maxFeePerGas,
		MaxPriorityFeePerGas: maxPriorityFeePerGas,
	})

	assert.NoError(err)
	assert.Equal(newTxID, r.TxID)
	assert.NotEmpty(r.TxHash)

	txAPIMock.AssertExpectations(t)
}
