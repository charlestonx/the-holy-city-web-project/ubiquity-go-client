package ubiquitytx

import (
	"context"
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"math/big"
	"strings"

	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const (
	ethereumGoerli     = "goerli"
	ethereumTxGasLimit = 21000 // Default for simple transfer transaction
)

var (
	ethereumChains = map[string]*big.Int{
		networkMainnet: big.NewInt(1),
		ethereumGoerli: big.NewInt(5),
	}
)

type EthereumTransaction struct {
	Network              string
	PrivateKey           string
	To                   string
	Amount               *big.Float // In Eth
	Nonce                *int64     // Current transaction count
	Gas                  *big.Int
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int
}

func (t EthereumTransaction) validate() error {
	if strings.TrimSpace(t.Network) == "" {
		return fmt.Errorf("field 'Network' is required")
	}
	if strings.TrimSpace(t.PrivateKey) == "" {
		return fmt.Errorf("field 'PrivateKey' is required")
	}
	if strings.TrimSpace(t.To) == "" {
		return fmt.Errorf("field 'To' is required")
	}
	if t.Amount == nil {
		return fmt.Errorf("field 'Amount' is required")
	}
	return nil
}

// SendETH creates, signs and sends ETH transaction. Under the hood it uses Ubiquity TxSend API.
func (s UbiquityTransactionService) SendETH(ctx context.Context, tx *EthereumTransaction) (*SendTxResult, error) {
	if err := tx.validate(); err != nil {
		return nil, fmt.Errorf("ethereum transaction validation failure: %v", err)
	}

	privKey, err := crypto.HexToECDSA(tx.PrivateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key: %v", err)
	}

	pubKey, ok := privKey.Public().(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("unexpected public key type: %T", pubKey)
	}

	from := crypto.PubkeyToAddress(*pubKey)
	var gas, maxFeePerGas, maxPriorityFeePerGas *int64
	if tx.Gas != nil {
		gas = toPointer(tx.Gas.Int64())
	}

	if tx.MaxPriorityFeePerGas == nil || tx.MaxFeePerGas == nil {
		estimatedMaxFeePerGas, estimatedMaxPriorityFeePerGas, err := s.estimateEthereumFees(ctx, tx.Network)
		if err != nil {
			return nil, fmt.Errorf("failed to estimate fees: %w", err)
		}

		if tx.MaxFeePerGas == nil {
			tx.MaxFeePerGas = estimatedMaxFeePerGas
		}

		if tx.MaxPriorityFeePerGas == nil {
			tx.MaxPriorityFeePerGas = estimatedMaxPriorityFeePerGas
		}
	}

	if tx.MaxFeePerGas != nil {
		maxFeePerGas = toPointer(tx.MaxFeePerGas.Int64())
	}

	if tx.MaxPriorityFeePerGas != nil {
		maxPriorityFeePerGas = toPointer(tx.MaxPriorityFeePerGas.Int64())
	}

	txRes, resp, err := s.txAPI.
		TxCreate(ctx, "ethereum", tx.Network).
		TxCreate(ubiquity.TxCreate{
			From: from.Hex(),
			To: []ubiquity.TxDestination{{
				Destination: tx.To,
				Amount:      tx.Amount.String(),
			}},
			Index: tx.Nonce,
			Protocol: &ubiquity.TxCreateProtocol{
				Ethereum: &ubiquity.TxCreateEvm{
					Gas:                  gas,
					MaxPriorityFeePerGas: maxPriorityFeePerGas,
					MaxFeePerGas:         maxFeePerGas,
				},
			},
		}).
		Execute()

	if err != nil && resp != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	} else if err != nil {
		return nil, fmt.Errorf("ubiquity TxCreate failure: %w", err)
	}

	txHash, signedTxData, err := SignEthereumTransaction(txRes.UnsignedTx, tx.Network, privKey)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal: %v", err)
	}

	txReceipt, resp, err := s.txAPI.TxSend(ctx, "ethereum", tx.Network).
		SignedTx(ubiquity.SignedTx{Tx: hex.EncodeToString(signedTxData)}).Execute()
	if err != nil {
		return nil, fmt.Errorf("ubiquity TxSend failure: resp. status = '%v' and err = '%v'", resp.Status, err)
	}

	return &SendTxResult{
		TxHash: txHash,
		TxID:   txReceipt.GetId(),
	}, nil
}

func (s UbiquityTransactionService) estimateEthereumFees(ctx context.Context, network string) (maxFeePerGas *big.Int, maxPriorityFeePerGas *big.Int, err error) {
	maxFeePerGas = new(big.Int)
	maxPriorityFeePerGas = new(big.Int)
	feeResp, resp, err := s.feesAPI.GetGasFeeEstimate(ctx, "ethereum", network).Execute()
	if err != nil {
		err = fmt.Errorf("ubiquity EstimateFee failure: resp. status = '%v' and err = '%v'",
			resp.Status,
			err,
		)
		return
	}

	// TODO: add logic for fast, slow or medium by param. Default: medium
	parseErr := fmt.Errorf("failed to parse fee: fee response does not follow expected format")
	mediumFeeInterface, ok := feeResp.EstimatedFees.Medium.(map[string]interface{})
	if !ok {
		err = parseErr
		return
	}

	// extract maxFeePerGas
	if maxFeePerGasInterface, ok := mediumFeeInterface["max_total_fee"]; ok {
		if maxFeePerGasFloat, ok := maxFeePerGasInterface.(float64); ok {
			maxFeePerGas.SetInt64(int64(maxFeePerGasFloat))
		} else {
			err = parseErr
		}
	} else {
		err = parseErr
	}

	// extract maxPriorityFeePerGas
	if maxPriorityFeePerGasInterface, ok := mediumFeeInterface["max_priority_fee"]; ok {
		if maxPriorityFeePerGasFloat, ok := maxPriorityFeePerGasInterface.(float64); ok {
			maxPriorityFeePerGas.SetInt64(int64(maxPriorityFeePerGasFloat))
		} else {
			err = parseErr
		}
	} else {
		err = parseErr
	}

	return
}

// SignEthereumTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a post London hard fork transaction, als referred to as an EIP1559 transaction.
func SignEthereumTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := ethereumChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewLondonSigner(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}

// SignEthereumLegacyTransaction signs an unsigned hex encoded transaction for the desired 'network'.
// This function signs a legacy transaction, these are transactions commonly used before the London hard fork
func SignEthereumLegacyTransaction(unsignedHexTx string, network string, privKey *ecdsa.PrivateKey) (txHash string, signedTx []byte, err error) {
	chainID, ok := ethereumChains[network]
	if !ok {
		return "", nil, fmt.Errorf("network %s is not supported", network)
	}

	signer := types.NewEIP2930Signer(chainID)
	return SignEVMTransactionWithSigner(unsignedHexTx, privKey, signer)
}
