package ubiquitytx

import (
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

const (
	networkTestnet = "testnet"
	networkMainnet = "mainnet"
)

type SendTxResult struct {
	TxHash string
	TxID   string
}

// UbiquityTransactionService provides API to send various protocol transactions.
// So far it supports BTC and ETH.
type UbiquityTransactionService struct {
	txAPI   ubiquity.TransactionsAPI
	feesAPI ubiquity.GasEstimatorAPI
}

func NewService(txAPI ubiquity.TransactionsAPI, feesAPI ubiquity.GasEstimatorAPI) *UbiquityTransactionService {
	return &UbiquityTransactionService{
		txAPI:   txAPI,
		feesAPI: feesAPI,
	}
}

type TxInput struct {
	Source string // UTXO, input transaction id
	Index  uint32 // UTXO, input index
}

type TxOutput struct {
	Destination string // Destination address
	Amount      int64  // In sat.
}

func toPointer[T any](a T) *T {
	return &a
}
