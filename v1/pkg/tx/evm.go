package ubiquitytx

import (
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"

	"github.com/ethereum/go-ethereum/core/types"
)

// SignEVMTransactionWithSigner signs an unsigned hex transaction using the provided private key and signer
// This will work for all Ethereum Virtual Machine (EVM) compliant blockchains, including Ethereum and Polygon
// This function allows more control. For higher level functions use 'SignEthereumTransaction' and 'SignPolygonTransaction'
func SignEVMTransactionWithSigner(unsignedHexTx string, privKey *ecdsa.PrivateKey, signer types.Signer) (txHash string, signedTx []byte, err error) {
	rawTx, err := hex.DecodeString(unsignedHexTx)
	if err != nil {
		return "", nil, fmt.Errorf("failed to decode hex unsigned tx: %w", err)
	}

	unsignedTx := new(types.Transaction)
	if err := unsignedTx.UnmarshalBinary(rawTx); err != nil {
		return "", nil, fmt.Errorf("faile to decode raw unsigned tx: %w", err)
	}

	signedRawTx, err := types.SignTx(unsignedTx, signer, privKey)
	if err != nil {
		return "", nil, fmt.Errorf("failed to sign: %v", err)
	}

	txHash = signedRawTx.Hash().Hex()
	signedTx, err = signedRawTx.MarshalBinary()
	return
}
