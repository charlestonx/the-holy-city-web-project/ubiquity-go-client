# FeeEstimate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MostRecentBlock** | Pointer to **int32** | Most recent block | [optional] 
**EstimatedFees** | Pointer to [**FeeEstimateEstimatedFees**](FeeEstimateEstimatedFees.md) |  | [optional] 

## Methods

### NewFeeEstimate

`func NewFeeEstimate() *FeeEstimate`

NewFeeEstimate instantiates a new FeeEstimate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewFeeEstimateWithDefaults

`func NewFeeEstimateWithDefaults() *FeeEstimate`

NewFeeEstimateWithDefaults instantiates a new FeeEstimate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetMostRecentBlock

`func (o *FeeEstimate) GetMostRecentBlock() int32`

GetMostRecentBlock returns the MostRecentBlock field if non-nil, zero value otherwise.

### GetMostRecentBlockOk

`func (o *FeeEstimate) GetMostRecentBlockOk() (*int32, bool)`

GetMostRecentBlockOk returns a tuple with the MostRecentBlock field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMostRecentBlock

`func (o *FeeEstimate) SetMostRecentBlock(v int32)`

SetMostRecentBlock sets MostRecentBlock field to given value.

### HasMostRecentBlock

`func (o *FeeEstimate) HasMostRecentBlock() bool`

HasMostRecentBlock returns a boolean if a field has been set.

### GetEstimatedFees

`func (o *FeeEstimate) GetEstimatedFees() FeeEstimateEstimatedFees`

GetEstimatedFees returns the EstimatedFees field if non-nil, zero value otherwise.

### GetEstimatedFeesOk

`func (o *FeeEstimate) GetEstimatedFeesOk() (*FeeEstimateEstimatedFees, bool)`

GetEstimatedFeesOk returns a tuple with the EstimatedFees field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEstimatedFees

`func (o *FeeEstimate) SetEstimatedFees(v FeeEstimateEstimatedFees)`

SetEstimatedFees sets EstimatedFees field to given value.

### HasEstimatedFees

`func (o *FeeEstimate) HasEstimatedFees() bool`

HasEstimatedFees returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


