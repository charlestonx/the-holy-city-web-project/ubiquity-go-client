# TxOutputResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Index** | Pointer to **int32** | the output index within a given transaction | [optional] 
**TxId** | Pointer to **string** | the transaction identifier | [optional] 
**Date** | Pointer to **int32** | the transaction creation unix timestamp | [optional] 
**BlockId** | Pointer to **string** | the hash identifier of the block which the transaction was mined | [optional] 
**BlockNumber** | Pointer to **int32** | the number of the block which the transaction was mined | [optional] 
**Confirmations** | Pointer to **int32** | the number of confirmations the transaction took in order to be mined | [optional] 
**Meta** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewTxOutputResponse

`func NewTxOutputResponse() *TxOutputResponse`

NewTxOutputResponse instantiates a new TxOutputResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxOutputResponseWithDefaults

`func NewTxOutputResponseWithDefaults() *TxOutputResponse`

NewTxOutputResponseWithDefaults instantiates a new TxOutputResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetIndex

`func (o *TxOutputResponse) GetIndex() int32`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *TxOutputResponse) GetIndexOk() (*int32, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *TxOutputResponse) SetIndex(v int32)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *TxOutputResponse) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetTxId

`func (o *TxOutputResponse) GetTxId() string`

GetTxId returns the TxId field if non-nil, zero value otherwise.

### GetTxIdOk

`func (o *TxOutputResponse) GetTxIdOk() (*string, bool)`

GetTxIdOk returns a tuple with the TxId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTxId

`func (o *TxOutputResponse) SetTxId(v string)`

SetTxId sets TxId field to given value.

### HasTxId

`func (o *TxOutputResponse) HasTxId() bool`

HasTxId returns a boolean if a field has been set.

### GetDate

`func (o *TxOutputResponse) GetDate() int32`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *TxOutputResponse) GetDateOk() (*int32, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *TxOutputResponse) SetDate(v int32)`

SetDate sets Date field to given value.

### HasDate

`func (o *TxOutputResponse) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetBlockId

`func (o *TxOutputResponse) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *TxOutputResponse) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *TxOutputResponse) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *TxOutputResponse) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### GetBlockNumber

`func (o *TxOutputResponse) GetBlockNumber() int32`

GetBlockNumber returns the BlockNumber field if non-nil, zero value otherwise.

### GetBlockNumberOk

`func (o *TxOutputResponse) GetBlockNumberOk() (*int32, bool)`

GetBlockNumberOk returns a tuple with the BlockNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockNumber

`func (o *TxOutputResponse) SetBlockNumber(v int32)`

SetBlockNumber sets BlockNumber field to given value.

### HasBlockNumber

`func (o *TxOutputResponse) HasBlockNumber() bool`

HasBlockNumber returns a boolean if a field has been set.

### GetConfirmations

`func (o *TxOutputResponse) GetConfirmations() int32`

GetConfirmations returns the Confirmations field if non-nil, zero value otherwise.

### GetConfirmationsOk

`func (o *TxOutputResponse) GetConfirmationsOk() (*int32, bool)`

GetConfirmationsOk returns a tuple with the Confirmations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmations

`func (o *TxOutputResponse) SetConfirmations(v int32)`

SetConfirmations sets Confirmations field to given value.

### HasConfirmations

`func (o *TxOutputResponse) HasConfirmations() bool`

HasConfirmations returns a boolean if a field has been set.

### GetMeta

`func (o *TxOutputResponse) GetMeta() map[string]interface{}`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *TxOutputResponse) GetMetaOk() (*map[string]interface{}, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *TxOutputResponse) SetMeta(v map[string]interface{})`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *TxOutputResponse) HasMeta() bool`

HasMeta returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


