# Currency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssetPath** | **string** | Asset path of transferred currency | 
**Symbol** | Pointer to **string** | Currency symbol | [optional] 
**Name** | Pointer to **string** | Name of currency | [optional] 
**Decimals** | Pointer to **int32** | Decimal places right to the comma | [optional] 
**Type** | **string** |  | [default to "smart_token"]
**Detail** | Pointer to [**SmartToken**](SmartToken.md) |  | [optional] 

## Methods

### NewCurrency

`func NewCurrency(assetPath string, type_ string, ) *Currency`

NewCurrency instantiates a new Currency object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCurrencyWithDefaults

`func NewCurrencyWithDefaults() *Currency`

NewCurrencyWithDefaults instantiates a new Currency object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAssetPath

`func (o *Currency) GetAssetPath() string`

GetAssetPath returns the AssetPath field if non-nil, zero value otherwise.

### GetAssetPathOk

`func (o *Currency) GetAssetPathOk() (*string, bool)`

GetAssetPathOk returns a tuple with the AssetPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssetPath

`func (o *Currency) SetAssetPath(v string)`

SetAssetPath sets AssetPath field to given value.


### GetSymbol

`func (o *Currency) GetSymbol() string`

GetSymbol returns the Symbol field if non-nil, zero value otherwise.

### GetSymbolOk

`func (o *Currency) GetSymbolOk() (*string, bool)`

GetSymbolOk returns a tuple with the Symbol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSymbol

`func (o *Currency) SetSymbol(v string)`

SetSymbol sets Symbol field to given value.

### HasSymbol

`func (o *Currency) HasSymbol() bool`

HasSymbol returns a boolean if a field has been set.

### GetName

`func (o *Currency) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Currency) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Currency) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *Currency) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDecimals

`func (o *Currency) GetDecimals() int32`

GetDecimals returns the Decimals field if non-nil, zero value otherwise.

### GetDecimalsOk

`func (o *Currency) GetDecimalsOk() (*int32, bool)`

GetDecimalsOk returns a tuple with the Decimals field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDecimals

`func (o *Currency) SetDecimals(v int32)`

SetDecimals sets Decimals field to given value.

### HasDecimals

`func (o *Currency) HasDecimals() bool`

HasDecimals returns a boolean if a field has been set.

### GetType

`func (o *Currency) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *Currency) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *Currency) SetType(v string)`

SetType sets Type field to given value.


### GetDetail

`func (o *Currency) GetDetail() SmartToken`

GetDetail returns the Detail field if non-nil, zero value otherwise.

### GetDetailOk

`func (o *Currency) GetDetailOk() (*SmartToken, bool)`

GetDetailOk returns a tuple with the Detail field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDetail

`func (o *Currency) SetDetail(v SmartToken)`

SetDetail sets Detail field to given value.

### HasDetail

`func (o *Currency) HasDetail() bool`

HasDetail returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


