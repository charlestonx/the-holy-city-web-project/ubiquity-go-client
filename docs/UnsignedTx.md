# UnsignedTx

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | The transaction ID | [optional] 
**UnsignedTx** | **string** | The transaction data needed to sign | 
**Meta** | Pointer to **map[string]interface{}** | Any extra information relevant regarding the created transaction | [optional] 

## Methods

### NewUnsignedTx

`func NewUnsignedTx(unsignedTx string, ) *UnsignedTx`

NewUnsignedTx instantiates a new UnsignedTx object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUnsignedTxWithDefaults

`func NewUnsignedTxWithDefaults() *UnsignedTx`

NewUnsignedTxWithDefaults instantiates a new UnsignedTx object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *UnsignedTx) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *UnsignedTx) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *UnsignedTx) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *UnsignedTx) HasId() bool`

HasId returns a boolean if a field has been set.

### GetUnsignedTx

`func (o *UnsignedTx) GetUnsignedTx() string`

GetUnsignedTx returns the UnsignedTx field if non-nil, zero value otherwise.

### GetUnsignedTxOk

`func (o *UnsignedTx) GetUnsignedTxOk() (*string, bool)`

GetUnsignedTxOk returns a tuple with the UnsignedTx field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetUnsignedTx

`func (o *UnsignedTx) SetUnsignedTx(v string)`

SetUnsignedTx sets UnsignedTx field to given value.


### GetMeta

`func (o *UnsignedTx) GetMeta() map[string]interface{}`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *UnsignedTx) GetMetaOk() (*map[string]interface{}, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *UnsignedTx) SetMeta(v map[string]interface{})`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *UnsignedTx) HasMeta() bool`

HasMeta returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


