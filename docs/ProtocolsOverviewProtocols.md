# ProtocolsOverviewProtocols

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Handle** | Pointer to **string** |  | [optional] 
**Network** | Pointer to **string** |  | [optional] 

## Methods

### NewProtocolsOverviewProtocols

`func NewProtocolsOverviewProtocols() *ProtocolsOverviewProtocols`

NewProtocolsOverviewProtocols instantiates a new ProtocolsOverviewProtocols object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProtocolsOverviewProtocolsWithDefaults

`func NewProtocolsOverviewProtocolsWithDefaults() *ProtocolsOverviewProtocols`

NewProtocolsOverviewProtocolsWithDefaults instantiates a new ProtocolsOverviewProtocols object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHandle

`func (o *ProtocolsOverviewProtocols) GetHandle() string`

GetHandle returns the Handle field if non-nil, zero value otherwise.

### GetHandleOk

`func (o *ProtocolsOverviewProtocols) GetHandleOk() (*string, bool)`

GetHandleOk returns a tuple with the Handle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHandle

`func (o *ProtocolsOverviewProtocols) SetHandle(v string)`

SetHandle sets Handle field to given value.

### HasHandle

`func (o *ProtocolsOverviewProtocols) HasHandle() bool`

HasHandle returns a boolean if a field has been set.

### GetNetwork

`func (o *ProtocolsOverviewProtocols) GetNetwork() string`

GetNetwork returns the Network field if non-nil, zero value otherwise.

### GetNetworkOk

`func (o *ProtocolsOverviewProtocols) GetNetworkOk() (*string, bool)`

GetNetworkOk returns a tuple with the Network field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNetwork

`func (o *ProtocolsOverviewProtocols) SetNetwork(v string)`

SetNetwork sets Network field to given value.

### HasNetwork

`func (o *ProtocolsOverviewProtocols) HasNetwork() bool`

HasNetwork returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


