# BlockIdentifiers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** | Number of items in block identifiers | [optional] 
**Data** | Pointer to [**[]BlockIdentifier**](BlockIdentifier.md) |  | [optional] 
**Meta** | Pointer to [**NullableMeta**](Meta.md) |  | [optional] 

## Methods

### NewBlockIdentifiers

`func NewBlockIdentifiers() *BlockIdentifiers`

NewBlockIdentifiers instantiates a new BlockIdentifiers object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBlockIdentifiersWithDefaults

`func NewBlockIdentifiersWithDefaults() *BlockIdentifiers`

NewBlockIdentifiersWithDefaults instantiates a new BlockIdentifiers object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *BlockIdentifiers) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *BlockIdentifiers) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *BlockIdentifiers) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *BlockIdentifiers) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetData

`func (o *BlockIdentifiers) GetData() []BlockIdentifier`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *BlockIdentifiers) GetDataOk() (*[]BlockIdentifier, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *BlockIdentifiers) SetData(v []BlockIdentifier)`

SetData sets Data field to given value.

### HasData

`func (o *BlockIdentifiers) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMeta

`func (o *BlockIdentifiers) GetMeta() Meta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *BlockIdentifiers) GetMetaOk() (*Meta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *BlockIdentifiers) SetMeta(v Meta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *BlockIdentifiers) HasMeta() bool`

HasMeta returns a boolean if a field has been set.

### SetMetaNil

`func (o *BlockIdentifiers) SetMetaNil(b bool)`

 SetMetaNil sets the value for Meta to be an explicit nil

### UnsetMeta
`func (o *BlockIdentifiers) UnsetMeta()`

UnsetMeta ensures that no value is present for Meta, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


