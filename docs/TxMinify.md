# TxMinify

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Unique transaction identifier | [optional] 
**Date** | Pointer to **int64** | Unix timestamp | [optional] 
**BlockId** | Pointer to **NullableString** | ID of block. | [optional] 
**BlockNumber** | Pointer to **NullableInt64** | Height of block, | [optional] 
**Confirmations** | Pointer to **int64** | Total transaction confirmations | [optional] 

## Methods

### NewTxMinify

`func NewTxMinify() *TxMinify`

NewTxMinify instantiates a new TxMinify object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxMinifyWithDefaults

`func NewTxMinifyWithDefaults() *TxMinify`

NewTxMinifyWithDefaults instantiates a new TxMinify object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *TxMinify) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *TxMinify) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *TxMinify) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *TxMinify) HasId() bool`

HasId returns a boolean if a field has been set.

### GetDate

`func (o *TxMinify) GetDate() int64`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *TxMinify) GetDateOk() (*int64, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *TxMinify) SetDate(v int64)`

SetDate sets Date field to given value.

### HasDate

`func (o *TxMinify) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetBlockId

`func (o *TxMinify) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *TxMinify) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *TxMinify) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *TxMinify) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### SetBlockIdNil

`func (o *TxMinify) SetBlockIdNil(b bool)`

 SetBlockIdNil sets the value for BlockId to be an explicit nil

### UnsetBlockId
`func (o *TxMinify) UnsetBlockId()`

UnsetBlockId ensures that no value is present for BlockId, not even an explicit nil
### GetBlockNumber

`func (o *TxMinify) GetBlockNumber() int64`

GetBlockNumber returns the BlockNumber field if non-nil, zero value otherwise.

### GetBlockNumberOk

`func (o *TxMinify) GetBlockNumberOk() (*int64, bool)`

GetBlockNumberOk returns a tuple with the BlockNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockNumber

`func (o *TxMinify) SetBlockNumber(v int64)`

SetBlockNumber sets BlockNumber field to given value.

### HasBlockNumber

`func (o *TxMinify) HasBlockNumber() bool`

HasBlockNumber returns a boolean if a field has been set.

### SetBlockNumberNil

`func (o *TxMinify) SetBlockNumberNil(b bool)`

 SetBlockNumberNil sets the value for BlockNumber to be an explicit nil

### UnsetBlockNumber
`func (o *TxMinify) UnsetBlockNumber()`

UnsetBlockNumber ensures that no value is present for BlockNumber, not even an explicit nil
### GetConfirmations

`func (o *TxMinify) GetConfirmations() int64`

GetConfirmations returns the Confirmations field if non-nil, zero value otherwise.

### GetConfirmationsOk

`func (o *TxMinify) GetConfirmationsOk() (*int64, bool)`

GetConfirmationsOk returns a tuple with the Confirmations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmations

`func (o *TxMinify) SetConfirmations(v int64)`

SetConfirmations sets Confirmations field to given value.

### HasConfirmations

`func (o *TxMinify) HasConfirmations() bool`

HasConfirmations returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


