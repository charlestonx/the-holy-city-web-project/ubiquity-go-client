# TxOutputs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** | Number of total items | [optional] 
**Data** | Pointer to [**[]TxOutputsData**](TxOutputsData.md) |  | [optional] 
**Meta** | Pointer to [**NullableMeta**](Meta.md) |  | [optional] 

## Methods

### NewTxOutputs

`func NewTxOutputs() *TxOutputs`

NewTxOutputs instantiates a new TxOutputs object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxOutputsWithDefaults

`func NewTxOutputsWithDefaults() *TxOutputs`

NewTxOutputsWithDefaults instantiates a new TxOutputs object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *TxOutputs) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *TxOutputs) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *TxOutputs) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *TxOutputs) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetData

`func (o *TxOutputs) GetData() []TxOutputsData`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *TxOutputs) GetDataOk() (*[]TxOutputsData, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *TxOutputs) SetData(v []TxOutputsData)`

SetData sets Data field to given value.

### HasData

`func (o *TxOutputs) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMeta

`func (o *TxOutputs) GetMeta() Meta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *TxOutputs) GetMetaOk() (*Meta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *TxOutputs) SetMeta(v Meta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *TxOutputs) HasMeta() bool`

HasMeta returns a boolean if a field has been set.

### SetMetaNil

`func (o *TxOutputs) SetMetaNil(b bool)`

 SetMetaNil sets the value for Meta to be an explicit nil

### UnsetMeta
`func (o *TxOutputs) UnsetMeta()`

UnsetMeta ensures that no value is present for Meta, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


