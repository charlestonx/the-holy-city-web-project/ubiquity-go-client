# \ProtocolAndEndpointSupportAPI

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetProtocolEndpoints**](ProtocolAndEndpointSupportAPI.md#GetProtocolEndpoints) | **Get** /{protocol}/{network} | Get the Protocol Info
[**GetProtocolsList**](ProtocolAndEndpointSupportAPI.md#GetProtocolsList) | **Get** / | Get the Protocols Overview



## GetProtocolEndpoints

> ProtocolDetail GetProtocolEndpoints(ctx, protocol, network).Execute()

Get the Protocol Info



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProtocolAndEndpointSupportAPI.GetProtocolEndpoints(context.Background(), protocol, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProtocolAndEndpointSupportAPI.GetProtocolEndpoints``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProtocolEndpoints`: ProtocolDetail
    fmt.Fprintf(os.Stdout, "Response from `ProtocolAndEndpointSupportAPI.GetProtocolEndpoints`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetProtocolEndpointsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**ProtocolDetail**](ProtocolDetail.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetProtocolsList

> ProtocolsOverview GetProtocolsList(ctx).Execute()

Get the Protocols Overview



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.ProtocolAndEndpointSupportAPI.GetProtocolsList(context.Background()).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `ProtocolAndEndpointSupportAPI.GetProtocolsList``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetProtocolsList`: ProtocolsOverview
    fmt.Fprintf(os.Stdout, "Response from `ProtocolAndEndpointSupportAPI.GetProtocolsList`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetProtocolsListRequest struct via the builder pattern


### Return type

[**ProtocolsOverview**](ProtocolsOverview.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

