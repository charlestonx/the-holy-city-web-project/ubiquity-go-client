# \BlocksAPI

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetBlockByNumber**](BlocksAPI.md#GetBlockByNumber) | **Get** /{protocol}/{network}/block/{block_identifier} | Get a Block by Number or Hash
[**GetBlockIdentifierByNumber**](BlocksAPI.md#GetBlockIdentifierByNumber) | **Get** /{protocol}/{network}/block_identifier/{block_identifier} | Get a Block Identifier by Number
[**GetBlockIdentifiers**](BlocksAPI.md#GetBlockIdentifiers) | **Get** /{protocol}/{network}/block_identifiers | Get a List of Block Identifiers
[**GetCurrentBlockHash**](BlocksAPI.md#GetCurrentBlockHash) | **Get** /{protocol}/{network}/sync/block_id | Get the Current Block Hash
[**GetCurrentBlockNumber**](BlocksAPI.md#GetCurrentBlockNumber) | **Get** /{protocol}/{network}/sync/block_number | Get the Current Block Number



## GetBlockByNumber

> Block GetBlockByNumber(ctx, protocol, network, blockIdentifier).Execute()

Get a Block by Number or Hash



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    blockIdentifier := "0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959" // string | The block identifier, hash or number. (default to "0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetBlockByNumber(context.Background(), protocol, network, blockIdentifier).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetBlockByNumber``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBlockByNumber`: Block
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetBlockByNumber`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**blockIdentifier** | **string** | The block identifier, hash or number. | [default to &quot;0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetBlockByNumberRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Block**](Block.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBlockIdentifierByNumber

> BlockIdentifier GetBlockIdentifierByNumber(ctx, protocol, network, blockIdentifier).Execute()

Get a Block Identifier by Number



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    blockIdentifier := "0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959" // string | The block identifier, hash or number. (default to "0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetBlockIdentifierByNumber(context.Background(), protocol, network, blockIdentifier).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetBlockIdentifierByNumber``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBlockIdentifierByNumber`: BlockIdentifier
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetBlockIdentifierByNumber`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**blockIdentifier** | **string** | The block identifier, hash or number. | [default to &quot;0000000000000000000590fc0f3eba193a278534220b2b37e9849e1a770ca959&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetBlockIdentifierByNumberRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**BlockIdentifier**](BlockIdentifier.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBlockIdentifiers

> BlockIdentifiers GetBlockIdentifiers(ctx, protocol, network).Order(order).PageToken(pageToken).PageSize(pageSize).Execute()

Get a List of Block Identifiers



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    order := "order_example" // string | The pagination order. (optional) (default to "desc")
    pageToken := "8185.123" // string | The continuation token from earlier response. (optional)
    pageSize := int32(25) // int32 | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetBlockIdentifiers(context.Background(), protocol, network).Order(order).PageToken(pageToken).PageSize(pageSize).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetBlockIdentifiers``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBlockIdentifiers`: BlockIdentifiers
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetBlockIdentifiers`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetBlockIdentifiersRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **order** | **string** | The pagination order. | [default to &quot;desc&quot;]
 **pageToken** | **string** | The continuation token from earlier response. | 
 **pageSize** | **int32** | Max number of items to return in a response. Defaults to 25 and is capped at 100.  | 

### Return type

[**BlockIdentifiers**](BlockIdentifiers.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: image/png, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCurrentBlockHash

> string GetCurrentBlockHash(ctx, protocol, network).Execute()

Get the Current Block Hash



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetCurrentBlockHash(context.Background(), protocol, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetCurrentBlockHash``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCurrentBlockHash`: string
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetCurrentBlockHash`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetCurrentBlockHashRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**string**

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetCurrentBlockNumber

> int64 GetCurrentBlockNumber(ctx, protocol, network).Execute()

Get the Current Block Number



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetCurrentBlockNumber(context.Background(), protocol, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetCurrentBlockNumber``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetCurrentBlockNumber`: int64
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetCurrentBlockNumber`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetCurrentBlockNumberRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**int64**

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

