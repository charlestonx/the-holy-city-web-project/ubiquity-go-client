# \AccountsAPI

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetListOfBalancesByAddress**](AccountsAPI.md#GetListOfBalancesByAddress) | **Get** /{protocol}/{network}/account/{address} | Get a List of Balances for an Address
[**GetListOfBalancesByAddresses**](AccountsAPI.md#GetListOfBalancesByAddresses) | **Post** /{protocol}/{network}/accounts | Get a List of Balances for Multiple Adresses
[**GetReportByAddress**](AccountsAPI.md#GetReportByAddress) | **Get** /{protocol}/{network}/account/{address}/report | Get a Financial Report for an Address between a Time Period
[**GetTxsByAddress**](AccountsAPI.md#GetTxsByAddress) | **Get** /{protocol}/{network}/account/{address}/txs | Get a List of Transactions
[**GetUTXOByAccount**](AccountsAPI.md#GetUTXOByAccount) | **Get** /{protocol}/{network}/account/{address}/utxo | Get a List of Transaction Inputs and Outputs



## GetListOfBalancesByAddress

> []Balance GetListOfBalancesByAddress(ctx, protocol, network, address).Assets(assets).Execute()

Get a List of Balances for an Address



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    address := "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa" // string | The account address of the protocol. (default to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa")
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetListOfBalancesByAddress(context.Background(), protocol, network, address).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetListOfBalancesByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetListOfBalancesByAddress`: []Balance
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetListOfBalancesByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**address** | **string** | The account address of the protocol. | [default to &quot;1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetListOfBalancesByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**[]Balance**](Balance.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetListOfBalancesByAddresses

> map[string][]Balance GetListOfBalancesByAddresses(ctx, protocol, network).AccountsObj(accountsObj).Assets(assets).Execute()

Get a List of Balances for Multiple Adresses



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    accountsObj := *openapiclient.NewAccountsObj() // AccountsObj | 
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetListOfBalancesByAddresses(context.Background(), protocol, network).AccountsObj(accountsObj).Assets(assets).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetListOfBalancesByAddresses``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetListOfBalancesByAddresses`: map[string][]Balance
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetListOfBalancesByAddresses`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetListOfBalancesByAddressesRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **accountsObj** | [**AccountsObj**](AccountsObj.md) |  | 
 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 

### Return type

[**map[string][]Balance**](array.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetReportByAddress

> Report GetReportByAddress(ctx, protocol, network, address).From(from).To(to).PageToken(pageToken).PageSize(pageSize).Execute()

Get a Financial Report for an Address between a Time Period



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "polkadot" // string | Protocol handle, one of: `algorand`, `polkadot`, `tezos`.  (default to "polkadot")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    address := "12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL" // string | The account address of the protocol. (default to "12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL")
    from := int32(961846434) // int32 | Unix Timestamp from where to start (optional)
    to := int32(1119612834) // int32 | Unix Timestamp from where to end (optional)
    pageToken := "xyz" // string | Continuation token from earlier response (optional)
    pageSize := int32(1000) // int32 | Max number of items to return in a response. Defaults to 50k and is capped at 100k.  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetReportByAddress(context.Background(), protocol, network, address).From(from).To(to).PageToken(pageToken).PageSize(pageSize).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetReportByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetReportByAddress`: Report
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetReportByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of: &#x60;algorand&#x60;, &#x60;polkadot&#x60;, &#x60;tezos&#x60;.  | [default to &quot;polkadot&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**address** | **string** | The account address of the protocol. | [default to &quot;12yi4uHFbnSUryffXT7Xq92fbGC3iXvCs3vz9HjVgpb4sBvL&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetReportByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **from** | **int32** | Unix Timestamp from where to start | 
 **to** | **int32** | Unix Timestamp from where to end | 
 **pageToken** | **string** | Continuation token from earlier response | 
 **pageSize** | **int32** | Max number of items to return in a response. Defaults to 50k and is capped at 100k.  | 

### Return type

[**Report**](Report.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetTxsByAddress

> TxPage GetTxsByAddress(ctx, protocol, network, address).Assets(assets).From(from).To(to).Order(order).PageToken(pageToken).PageSize(pageSize).Execute()

Get a List of Transactions



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of:  `algorand`, `bitcoin`, `bitcoincash`, `dogecoin`, `ethereum`, `litecoin`, `near`, `oasis`, `optimism`, `polkadot`, `polygon`, `solana`, `stellar`, `tezos`, `xrp`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    address := "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa" // string | The account address of the protocol. (default to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa")
    assets := "ethereum/native/eth" // string | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. (optional)
    from := int32(961846434) // int32 | Unix Timestamp from where to start (optional)
    to := int32(1119612834) // int32 | Unix Timestamp from where to end (optional)
    order := "order_example" // string | The pagination order. (optional) (default to "desc")
    pageToken := "8185.123" // string | The continuation token from earlier response. (optional)
    pageSize := int32(25) // int32 | Max number of items to return in a response. Defaults to 25 and is capped at 100.  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetTxsByAddress(context.Background(), protocol, network, address).Assets(assets).From(from).To(to).Order(order).PageToken(pageToken).PageSize(pageSize).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetTxsByAddress``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetTxsByAddress`: TxPage
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetTxsByAddress`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of:  &#x60;algorand&#x60;, &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;, &#x60;near&#x60;, &#x60;oasis&#x60;, &#x60;optimism&#x60;, &#x60;polkadot&#x60;, &#x60;polygon&#x60;, &#x60;solana&#x60;, &#x60;stellar&#x60;, &#x60;tezos&#x60;, &#x60;xrp&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**address** | **string** | The account address of the protocol. | [default to &quot;1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetTxsByAddressRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **assets** | **string** | Comma-separated list of asset paths to filter. If the list is empty, or all elements are empty, this filter has no effect. | 
 **from** | **int32** | Unix Timestamp from where to start | 
 **to** | **int32** | Unix Timestamp from where to end | 
 **order** | **string** | The pagination order. | [default to &quot;desc&quot;]
 **pageToken** | **string** | The continuation token from earlier response. | 
 **pageSize** | **int32** | Max number of items to return in a response. Defaults to 25 and is capped at 100.  | 

### Return type

[**TxPage**](TxPage.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetUTXOByAccount

> TxOutputs GetUTXOByAccount(ctx, protocol, network, address).Spent(spent).CheckMempool(checkMempool).From(from).To(to).Order(order).PageToken(pageToken).PageSize(pageSize).Execute()

Get a List of Transaction Inputs and Outputs



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of: `bitcoin`, `bitcoincash`, `dogecoin`, `litecoin`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")
    address := "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa" // string | The account address of the protocol. (default to "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa")
    spent := true // bool | Whether the transaction output was spent or not (optional)
    checkMempool := true // bool | Whether to check for UTXOs spent in the mempool as well as UTXOs pending in the mempool (optional)
    from := int32(961846434) // int32 | Unix Timestamp from where to start (optional)
    to := int32(1119612834) // int32 | Unix Timestamp from where to end (optional)
    order := "order_example" // string | The pagination order. (optional) (default to "desc")
    pageToken := "8185.123" // string | The continuation token from earlier response. (optional)
    pageSize := int32(1000) // int32 | Max number of items to return in a response. Defaults to 50k and is capped at 100k.  (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.AccountsAPI.GetUTXOByAccount(context.Background(), protocol, network, address).Spent(spent).CheckMempool(checkMempool).From(from).To(to).Order(order).PageToken(pageToken).PageSize(pageSize).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `AccountsAPI.GetUTXOByAccount``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetUTXOByAccount`: TxOutputs
    fmt.Fprintf(os.Stdout, "Response from `AccountsAPI.GetUTXOByAccount`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of: &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;dogecoin&#x60;, &#x60;litecoin&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]
**address** | **string** | The account address of the protocol. | [default to &quot;1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetUTXOByAccountRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



 **spent** | **bool** | Whether the transaction output was spent or not | 
 **checkMempool** | **bool** | Whether to check for UTXOs spent in the mempool as well as UTXOs pending in the mempool | 
 **from** | **int32** | Unix Timestamp from where to start | 
 **to** | **int32** | Unix Timestamp from where to end | 
 **order** | **string** | The pagination order. | [default to &quot;desc&quot;]
 **pageToken** | **string** | The continuation token from earlier response. | 
 **pageSize** | **int32** | Max number of items to return in a response. Defaults to 50k and is capped at 100k.  | 

### Return type

[**TxOutputs**](TxOutputs.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, text/csv

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

