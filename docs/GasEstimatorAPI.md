# \GasEstimatorAPI

All URIs are relative to *https://svc.blockdaemon.com/universal/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetGasFeeEstimate**](GasEstimatorAPI.md#GetGasFeeEstimate) | **Get** /{protocol}/{network}/tx/estimate_fee | Get the Gas Fee Estimation



## GetGasFeeEstimate

> FeeEstimate GetGasFeeEstimate(ctx, protocol, network).Execute()

Get the Gas Fee Estimation



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "bitcoin" // string | Protocol handle, one of: `bitcoin`, `bitcoincash`, `ethereum`, `litecoin`.  (default to "bitcoin")
    network := "mainnet" // string | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. (default to "mainnet")

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.GasEstimatorAPI.GetGasFeeEstimate(context.Background(), protocol, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `GasEstimatorAPI.GetGasFeeEstimate``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetGasFeeEstimate`: FeeEstimate
    fmt.Fprintf(os.Stdout, "Response from `GasEstimatorAPI.GetGasFeeEstimate`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Protocol handle, one of: &#x60;bitcoin&#x60;, &#x60;bitcoincash&#x60;, &#x60;ethereum&#x60;, &#x60;litecoin&#x60;.  | [default to &quot;bitcoin&quot;]
**network** | **string** | Which network to target. Available networks can be found in the list of supported protocols or with /{protocol}. | [default to &quot;mainnet&quot;]

### Other Parameters

Other parameters are passed through a pointer to a apiGetGasFeeEstimateRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

[**FeeEstimate**](FeeEstimate.md)

### Authorization

[apiKeyAuthHeader](../README.md#apiKeyAuthHeader), [bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

