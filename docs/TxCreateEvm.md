# TxCreateEvm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Gas** | Pointer to **int64** | Gas of transaction. If omitted, the gas will be estimated using eth_estimateGas | [optional] 
**MaxPriorityFeePerGas** | Pointer to **int64** | The max priority fee per gas. Also referred to as the miner tip. When omitted an estimate will be used. | [optional] 
**MaxFeePerGas** | Pointer to **int64** | The max fee per gas, this includes the base fee and max priority fee per gas. When omitted an esitmate will be used. | [optional] 

## Methods

### NewTxCreateEvm

`func NewTxCreateEvm() *TxCreateEvm`

NewTxCreateEvm instantiates a new TxCreateEvm object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxCreateEvmWithDefaults

`func NewTxCreateEvmWithDefaults() *TxCreateEvm`

NewTxCreateEvmWithDefaults instantiates a new TxCreateEvm object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetGas

`func (o *TxCreateEvm) GetGas() int64`

GetGas returns the Gas field if non-nil, zero value otherwise.

### GetGasOk

`func (o *TxCreateEvm) GetGasOk() (*int64, bool)`

GetGasOk returns a tuple with the Gas field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGas

`func (o *TxCreateEvm) SetGas(v int64)`

SetGas sets Gas field to given value.

### HasGas

`func (o *TxCreateEvm) HasGas() bool`

HasGas returns a boolean if a field has been set.

### GetMaxPriorityFeePerGas

`func (o *TxCreateEvm) GetMaxPriorityFeePerGas() int64`

GetMaxPriorityFeePerGas returns the MaxPriorityFeePerGas field if non-nil, zero value otherwise.

### GetMaxPriorityFeePerGasOk

`func (o *TxCreateEvm) GetMaxPriorityFeePerGasOk() (*int64, bool)`

GetMaxPriorityFeePerGasOk returns a tuple with the MaxPriorityFeePerGas field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxPriorityFeePerGas

`func (o *TxCreateEvm) SetMaxPriorityFeePerGas(v int64)`

SetMaxPriorityFeePerGas sets MaxPriorityFeePerGas field to given value.

### HasMaxPriorityFeePerGas

`func (o *TxCreateEvm) HasMaxPriorityFeePerGas() bool`

HasMaxPriorityFeePerGas returns a boolean if a field has been set.

### GetMaxFeePerGas

`func (o *TxCreateEvm) GetMaxFeePerGas() int64`

GetMaxFeePerGas returns the MaxFeePerGas field if non-nil, zero value otherwise.

### GetMaxFeePerGasOk

`func (o *TxCreateEvm) GetMaxFeePerGasOk() (*int64, bool)`

GetMaxFeePerGasOk returns a tuple with the MaxFeePerGas field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMaxFeePerGas

`func (o *TxCreateEvm) SetMaxFeePerGas(v int64)`

SetMaxFeePerGas sets MaxFeePerGas field to given value.

### HasMaxFeePerGas

`func (o *TxCreateEvm) HasMaxFeePerGas() bool`

HasMaxFeePerGas returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


