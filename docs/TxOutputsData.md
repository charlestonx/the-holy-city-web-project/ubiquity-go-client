# TxOutputsData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | Pointer to **string** | the status of the given transaction output | [optional] 
**IsSpent** | Pointer to **bool** | whether the transaction output was spent or not | [optional] 
**Value** | Pointer to **int64** | the amount of tokens within the given output | [optional] 
**Spent** | Pointer to [**TxOutputResponse**](tx-output-response.md) |  | [optional] 
**Mined** | Pointer to [**TxOutputResponse**](tx-output-response.md) |  | [optional] 
**Pending** | Pointer to [**TxOutputResponse**](tx-output-response.md) |  | [optional] 

## Methods

### NewTxOutputsData

`func NewTxOutputsData() *TxOutputsData`

NewTxOutputsData instantiates a new TxOutputsData object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxOutputsDataWithDefaults

`func NewTxOutputsDataWithDefaults() *TxOutputsData`

NewTxOutputsDataWithDefaults instantiates a new TxOutputsData object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *TxOutputsData) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *TxOutputsData) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *TxOutputsData) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *TxOutputsData) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetIsSpent

`func (o *TxOutputsData) GetIsSpent() bool`

GetIsSpent returns the IsSpent field if non-nil, zero value otherwise.

### GetIsSpentOk

`func (o *TxOutputsData) GetIsSpentOk() (*bool, bool)`

GetIsSpentOk returns a tuple with the IsSpent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsSpent

`func (o *TxOutputsData) SetIsSpent(v bool)`

SetIsSpent sets IsSpent field to given value.

### HasIsSpent

`func (o *TxOutputsData) HasIsSpent() bool`

HasIsSpent returns a boolean if a field has been set.

### GetValue

`func (o *TxOutputsData) GetValue() int64`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *TxOutputsData) GetValueOk() (*int64, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *TxOutputsData) SetValue(v int64)`

SetValue sets Value field to given value.

### HasValue

`func (o *TxOutputsData) HasValue() bool`

HasValue returns a boolean if a field has been set.

### GetSpent

`func (o *TxOutputsData) GetSpent() TxOutputResponse`

GetSpent returns the Spent field if non-nil, zero value otherwise.

### GetSpentOk

`func (o *TxOutputsData) GetSpentOk() (*TxOutputResponse, bool)`

GetSpentOk returns a tuple with the Spent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSpent

`func (o *TxOutputsData) SetSpent(v TxOutputResponse)`

SetSpent sets Spent field to given value.

### HasSpent

`func (o *TxOutputsData) HasSpent() bool`

HasSpent returns a boolean if a field has been set.

### GetMined

`func (o *TxOutputsData) GetMined() TxOutputResponse`

GetMined returns the Mined field if non-nil, zero value otherwise.

### GetMinedOk

`func (o *TxOutputsData) GetMinedOk() (*TxOutputResponse, bool)`

GetMinedOk returns a tuple with the Mined field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMined

`func (o *TxOutputsData) SetMined(v TxOutputResponse)`

SetMined sets Mined field to given value.

### HasMined

`func (o *TxOutputsData) HasMined() bool`

HasMined returns a boolean if a field has been set.

### GetPending

`func (o *TxOutputsData) GetPending() TxOutputResponse`

GetPending returns the Pending field if non-nil, zero value otherwise.

### GetPendingOk

`func (o *TxOutputsData) GetPendingOk() (*TxOutputResponse, bool)`

GetPendingOk returns a tuple with the Pending field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPending

`func (o *TxOutputsData) SetPending(v TxOutputResponse)`

SetPending sets Pending field to given value.

### HasPending

`func (o *TxOutputsData) HasPending() bool`

HasPending returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


