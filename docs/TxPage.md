# TxPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** | Number of items in txs | [optional] 
**Data** | Pointer to [**[]Tx**](Tx.md) |  | [optional] 
**Meta** | Pointer to [**NullableMeta**](Meta.md) |  | [optional] 

## Methods

### NewTxPage

`func NewTxPage() *TxPage`

NewTxPage instantiates a new TxPage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxPageWithDefaults

`func NewTxPageWithDefaults() *TxPage`

NewTxPageWithDefaults instantiates a new TxPage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *TxPage) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *TxPage) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *TxPage) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *TxPage) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetData

`func (o *TxPage) GetData() []Tx`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *TxPage) GetDataOk() (*[]Tx, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *TxPage) SetData(v []Tx)`

SetData sets Data field to given value.

### HasData

`func (o *TxPage) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMeta

`func (o *TxPage) GetMeta() Meta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *TxPage) GetMetaOk() (*Meta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *TxPage) SetMeta(v Meta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *TxPage) HasMeta() bool`

HasMeta returns a boolean if a field has been set.

### SetMetaNil

`func (o *TxPage) SetMetaNil(b bool)`

 SetMetaNil sets the value for Meta to be an explicit nil

### UnsetMeta
`func (o *TxPage) UnsetMeta()`

UnsetMeta ensures that no value is present for Meta, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


