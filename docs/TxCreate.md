# TxCreate

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**From** | **string** | The source UTXO or account ID for the originating funds | 
**To** | [**[]TxDestination**](TxDestination.md) | A list of recipients | 
**Index** | Pointer to **int64** | The UTXO index or the account Nonce | [optional] 
**Fee** | Pointer to **string** | The fee you are willing to pay for the transaction. For Ethereum and Polygon see &#39;protocol.ethereum&#39; and &#39;protocol.polygon&#39;. | [optional] 
**Protocol** | Pointer to [**TxCreateProtocol**](TxCreateProtocol.md) |  | [optional] 

## Methods

### NewTxCreate

`func NewTxCreate(from string, to []TxDestination, ) *TxCreate`

NewTxCreate instantiates a new TxCreate object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxCreateWithDefaults

`func NewTxCreateWithDefaults() *TxCreate`

NewTxCreateWithDefaults instantiates a new TxCreate object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetFrom

`func (o *TxCreate) GetFrom() string`

GetFrom returns the From field if non-nil, zero value otherwise.

### GetFromOk

`func (o *TxCreate) GetFromOk() (*string, bool)`

GetFromOk returns a tuple with the From field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFrom

`func (o *TxCreate) SetFrom(v string)`

SetFrom sets From field to given value.


### GetTo

`func (o *TxCreate) GetTo() []TxDestination`

GetTo returns the To field if non-nil, zero value otherwise.

### GetToOk

`func (o *TxCreate) GetToOk() (*[]TxDestination, bool)`

GetToOk returns a tuple with the To field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTo

`func (o *TxCreate) SetTo(v []TxDestination)`

SetTo sets To field to given value.


### GetIndex

`func (o *TxCreate) GetIndex() int64`

GetIndex returns the Index field if non-nil, zero value otherwise.

### GetIndexOk

`func (o *TxCreate) GetIndexOk() (*int64, bool)`

GetIndexOk returns a tuple with the Index field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIndex

`func (o *TxCreate) SetIndex(v int64)`

SetIndex sets Index field to given value.

### HasIndex

`func (o *TxCreate) HasIndex() bool`

HasIndex returns a boolean if a field has been set.

### GetFee

`func (o *TxCreate) GetFee() string`

GetFee returns the Fee field if non-nil, zero value otherwise.

### GetFeeOk

`func (o *TxCreate) GetFeeOk() (*string, bool)`

GetFeeOk returns a tuple with the Fee field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFee

`func (o *TxCreate) SetFee(v string)`

SetFee sets Fee field to given value.

### HasFee

`func (o *TxCreate) HasFee() bool`

HasFee returns a boolean if a field has been set.

### GetProtocol

`func (o *TxCreate) GetProtocol() TxCreateProtocol`

GetProtocol returns the Protocol field if non-nil, zero value otherwise.

### GetProtocolOk

`func (o *TxCreate) GetProtocolOk() (*TxCreateProtocol, bool)`

GetProtocolOk returns a tuple with the Protocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocol

`func (o *TxCreate) SetProtocol(v TxCreateProtocol)`

SetProtocol sets Protocol field to given value.

### HasProtocol

`func (o *TxCreate) HasProtocol() bool`

HasProtocol returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


