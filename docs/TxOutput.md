# TxOutput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | Pointer to **string** | Result status of the transaction output. | [optional] 
**IsSpent** | Pointer to **bool** | If the transaction output was spent or not, if the value is true the &#x60;spent&#x60; transaction object will be presented | [optional] 
**Value** | Pointer to **NullableInt64** | Amount of transaction output | [optional] 
**Mined** | Pointer to [**NullableTxMinify**](TxMinify.md) |  | [optional] 
**Spent** | Pointer to [**NullableTxMinify**](TxMinify.md) |  | [optional] 

## Methods

### NewTxOutput

`func NewTxOutput() *TxOutput`

NewTxOutput instantiates a new TxOutput object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxOutputWithDefaults

`func NewTxOutputWithDefaults() *TxOutput`

NewTxOutputWithDefaults instantiates a new TxOutput object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetStatus

`func (o *TxOutput) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *TxOutput) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *TxOutput) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *TxOutput) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetIsSpent

`func (o *TxOutput) GetIsSpent() bool`

GetIsSpent returns the IsSpent field if non-nil, zero value otherwise.

### GetIsSpentOk

`func (o *TxOutput) GetIsSpentOk() (*bool, bool)`

GetIsSpentOk returns a tuple with the IsSpent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetIsSpent

`func (o *TxOutput) SetIsSpent(v bool)`

SetIsSpent sets IsSpent field to given value.

### HasIsSpent

`func (o *TxOutput) HasIsSpent() bool`

HasIsSpent returns a boolean if a field has been set.

### GetValue

`func (o *TxOutput) GetValue() int64`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *TxOutput) GetValueOk() (*int64, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *TxOutput) SetValue(v int64)`

SetValue sets Value field to given value.

### HasValue

`func (o *TxOutput) HasValue() bool`

HasValue returns a boolean if a field has been set.

### SetValueNil

`func (o *TxOutput) SetValueNil(b bool)`

 SetValueNil sets the value for Value to be an explicit nil

### UnsetValue
`func (o *TxOutput) UnsetValue()`

UnsetValue ensures that no value is present for Value, not even an explicit nil
### GetMined

`func (o *TxOutput) GetMined() TxMinify`

GetMined returns the Mined field if non-nil, zero value otherwise.

### GetMinedOk

`func (o *TxOutput) GetMinedOk() (*TxMinify, bool)`

GetMinedOk returns a tuple with the Mined field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMined

`func (o *TxOutput) SetMined(v TxMinify)`

SetMined sets Mined field to given value.

### HasMined

`func (o *TxOutput) HasMined() bool`

HasMined returns a boolean if a field has been set.

### SetMinedNil

`func (o *TxOutput) SetMinedNil(b bool)`

 SetMinedNil sets the value for Mined to be an explicit nil

### UnsetMined
`func (o *TxOutput) UnsetMined()`

UnsetMined ensures that no value is present for Mined, not even an explicit nil
### GetSpent

`func (o *TxOutput) GetSpent() TxMinify`

GetSpent returns the Spent field if non-nil, zero value otherwise.

### GetSpentOk

`func (o *TxOutput) GetSpentOk() (*TxMinify, bool)`

GetSpentOk returns a tuple with the Spent field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSpent

`func (o *TxOutput) SetSpent(v TxMinify)`

SetSpent sets Spent field to given value.

### HasSpent

`func (o *TxOutput) HasSpent() bool`

HasSpent returns a boolean if a field has been set.

### SetSpentNil

`func (o *TxOutput) SetSpentNil(b bool)`

 SetSpentNil sets the value for Spent to be an explicit nil

### UnsetSpent
`func (o *TxOutput) UnsetSpent()`

UnsetSpent ensures that no value is present for Spent, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


